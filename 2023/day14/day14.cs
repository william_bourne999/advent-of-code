﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day14
    {
        private enum Rock { Round, Cube, Empty }
        private enum Direction { North, West, South, East }

        public static void Main()
        {
            var input = File.ReadAllLines("2023/day14/input");
            var tile = input.Select(ParseRow).ToArray();

            // In Part One, tilt the tile to slide the rocks north then sum all the load values:
            TiltTile(tile, Direction.North);
            Console.WriteLine("Part One = {0}", SumLoadValues(tile));

            // In Part Two, we must spin cycle the tile 1000000000 times:
            var max = 1000000000;
            var history = new List<Rock[][]>();
            for (var n = 0; n < max; ++n)
            {
                var repeat = SpinTile(tile, history);
                if (repeat != -1)
                {
                    var cycle = n - repeat;
                    n += ((max - n) / cycle) * cycle;
                    history = null;
                }
            }
            Console.WriteLine("Part Two = {0}", SumLoadValues(tile));
        }

        private static int SpinTile(Rock[][] tile, List<Rock[][]> history)
        {
            TiltTile(tile, Direction.North);
            TiltTile(tile, Direction.West );
            TiltTile(tile, Direction.South);
            TiltTile(tile, Direction.East );

            if (history != null)
            {
                for (var n = 0; n < history.Count; ++n)
                {
                    if (AreTilesEqual(tile, history[n])) return n;
                }
                history.Add(tile.Select(x => x.ToArray()).ToArray());
            }
            return -1;
        }

        private static bool AreTilesEqual(Rock[][] first, Rock[][] second)
        {
            if (first.Length != second.Length) return false;
            for (var y = 0; y < first.Length; ++y)
            {
                var a = first[y];
                var b = second[y];
                if (a.Length != b.Length) return false;
                for (var x = 0; x < a.Length; ++x) if (a[x] != b[x]) return false;
            }
            return true;
        }

        private static void TiltTile(Rock[][] tile, Direction direction)
        {
            var changed   = true;
            var ascending = direction == Direction.North || direction == Direction.West;
            var vertical  = direction == Direction.North || direction == Direction.South;
            var max       = vertical ? tile.Length : tile[0].Length;
            if (vertical)
            {
                while (changed)
                {
                    changed = false;
                    for (var y = 1; y < max; ++y)
                    {
                        var curr = tile[ascending ? y : max - y - 1];
                        var prev = tile[ascending ? y - 1 : max - y];
                        for (var x = 0; x < curr.Length; ++x)
                        {
                            ref Rock current = ref curr[x];
                            ref Rock previous = ref prev[x];
                            if (current == Rock.Round && previous == Rock.Empty)
                            {
                                current  = Rock.Empty;
                                previous = Rock.Round;
                                changed  = true;
                            }
                        }
                    }
                }
            }
            else
            {
                while (changed)
                {
                    changed = false;
                    for (var x = 1; x < max; ++x)
                    {
                        var currX = ascending ? x : max - x - 1;
                        var prevX = ascending ? x - 1 : max - x;
                        for (var y = 0; y < tile.Length; ++y)
                        {
                            ref Rock current = ref tile[y][currX];
                            ref Rock previous = ref tile[y][prevX];
                            if (current == Rock.Round && previous == Rock.Empty)
                            {
                                current  = Rock.Empty;
                                previous = Rock.Round;
                                changed  = true;
                            }
                        }
                    }
                }
            }
        }

        private static int SumLoadValues(Rock[][] tile)
        {
            var load = 0;
            for (var y = 0; y < tile.Length; ++y)
            {
                var row = tile[y];
                for (var x = 0; x < row.Length; ++x)
                {
                    load += row[x] == Rock.Round ? tile.Length - y : 0;
                }
            }
            return load;
        }

        private static Rock[] ParseRow(string row)
        {
            return row.Select(ParseRock).ToArray();
        }

        private static Rock ParseRock(char c)
        {
            return c switch
            {
                'O' => Rock.Round,
                '#' => Rock.Cube,
                _ => Rock.Empty,
            };
        }
    }
}
