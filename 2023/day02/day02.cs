namespace AdventOfCode2023
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day02
    {
        private record Game(int ID, Draw[] Draws, Draw Max);
        private record Draw(int Red, int Green, int Blue);

        public static void Main()
        {
            var input = File.ReadAllLines("2023/day02/input");
            var games = input.Select(ParseGame).ToList();

            // In Part One, sum the IDs of the games which can be played with 12 red, 13 green and 14 blue balls:
            var partOne = games.Where(x => x.Draws.All(y => y.Red <= 12 && y.Green <= 13 && y.Blue <= 14)).Sum(x => x.ID);
            Console.WriteLine("Part One = {0}", partOne);

            // In Part Two, multiply the maximum observed balls for each game together, and sum them:
            var partTwo = games.Sum(x => x.Max.Red * x.Max.Green * x.Max.Blue);
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static Game ParseGame(string str)
        {
            var game = str.Split(": ");
            var id = int.Parse(game[0].Substring(5));
            var draws = game[1].Split("; ").Select(ParseDraw).ToArray();
            return new Game(id, draws, new Draw(draws.Max(x => x.Red), draws.Max(x => x.Green), draws.Max(x => x.Blue)));
        }

        private static Draw ParseDraw(string str)
        {
            var balls = str.Split(", ");
            return new Draw(ParseColour(balls, "red"), ParseColour(balls, "green"), ParseColour(balls, "blue"));
        }

        private static int ParseColour(string[] balls, string colour)
        {
            var entry = balls.FirstOrDefault(x => x.EndsWith(colour));
            return entry != null && int.TryParse(entry.Remove(entry.IndexOf(' ')), out var n) ? n : 0;
        }
    }
}
