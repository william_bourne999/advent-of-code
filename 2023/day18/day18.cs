﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day18
    {
        private sealed record Instruction(Vec2D Direction, int Distance);
        private static readonly Vec2D[] Directions = new Vec2D[] { new(1, 0), new(0, 1), new(-1, 0), new(0, -1) };

        public static void Main()
        {
            var input = File.ReadAllLines("2023/day18/input");
            var partOne = input.Select(ParsePartOne).ToArray();
            Console.WriteLine("Part One = {0}", FindLoopArea(partOne));

            var partTwo = input.Select(ParsePartTwo).ToArray();
            Console.WriteLine("Part Two = {0}", FindLoopArea(partTwo));
        }

        private static long FindLoopArea(IReadOnlyList<Instruction> instructions)
        {
            // Find all the vertices of the polygon, then use the shoelace formula to find the area:
            var perimeter = 0L;
            var pos = new Vec2D(0, 0);
            var vertices = new List<Vec2D>();
            foreach (var instruction in instructions)
            {
                vertices.Add(pos);
                perimeter += instruction.Distance;
                pos += instruction.Direction * instruction.Distance;
            }

            // The loop area is the shoelace area add half the perimeter to account for the squares we missed:
            var area = 0L;
            for (var n = 0; n < vertices.Count; ++n)
            {
                var prevN = n == 0 ? vertices.Count - 1 : n - 1;
                var nextN = n == vertices.Count - 1 ? 0 : n + 1;
                area += (long)vertices[n].Y * (vertices[prevN].X - vertices[nextN].X);
            }
            return 1 + (area + perimeter) / 2;
        }

        private static Instruction ParsePartOne(string str)
        {
            var parts = str.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var dir = Directions["RDLU".IndexOf(parts[0][0])];
            return new Instruction(dir, int.Parse(parts[1]));
        }

        private static Instruction ParsePartTwo(string str)
        {
            var parts = str.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var dir = Directions[parts[2][7] - '0'];
            var dist = int.Parse(parts[2][2..7], NumberStyles.HexNumber);
            return new Instruction(dir, dist);
        }
    }
}
