﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day17
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2023/day17/input");

            Console.WriteLine("Part One = {0}", 0);
        }
    }
}
