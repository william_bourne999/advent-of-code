namespace AdventOfCode2023
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day04
    {
        private record Card(int ID, int[] Winners, int[] Numbers, int WinCount);

        public static void Main()
        {
            var input = File.ReadAllLines("2023/day04/input");
            var cards = input.Select(ParseCard).ToArray();

            // For Part One, count the number of winning numbers for each card and sum the scores:
            var partOne = cards.Sum(x => x.WinCount > 0 ? 1 << (x.WinCount - 1) : 0);
            Console.WriteLine("Part One = {0}", partOne);

            // For Part Two, each winning number gives you copies of the cards below the current card:
            var counts = Enumerable.Repeat(1, cards.Length).ToArray();
            for (var n = 0; n < cards.Length; ++n)
            {
                var winCount = cards[n].WinCount;
                for (var i = n + 1; i < n + 1 + winCount && i < cards.Length; ++i) counts[i] += counts[n];
            }
            Console.WriteLine("Part Two = {0}", counts.Sum());
        }

        private static Card ParseCard(string str)
        {
            var card    = str.Split(':', StringSplitOptions.TrimEntries);
            var id      = int.Parse(card[0].Substring(5));
            var lists   = card[1].Split('|', StringSplitOptions.TrimEntries);
            var winners = ParseNumbers(lists[0]);
            var numbers = ParseNumbers(lists[1]);
            return new Card(id, winners, numbers, numbers.Count(winners.Contains));
        }

        private static int[] ParseNumbers(string str)
        {
            return str.Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
        }
    }
}
