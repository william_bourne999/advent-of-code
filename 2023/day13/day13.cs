﻿namespace AdventOfCode2023
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Numerics;

    public static class Day13
    {
        private const uint Zero = 0;
        private const uint One  = 1;

        public static void Main()
        {
            var input = File.ReadAllText("2023/day13/input");
            var maps = input.Split(Environment.NewLine + Environment.NewLine).Select(SplitLines).ToArray();
            var rows = maps.Select(ProcessRows).ToArray();
            var cols = maps.Select(ProcessColumns).ToArray();

            // In Part One, find the horizontal or vertical mirror for each map and sum the 1*columns and 100*rows:
            Console.WriteLine("Part One = {0}", SumMirrorsLocations(rows, cols, 0));

            // In Part Two, the mirrors have exactly one smudge, so expected differences is set to one:
            Console.WriteLine("Part Two = {0}", SumMirrorsLocations(rows, cols, 1));
        }

        private static int SumMirrorsLocations(uint[][]rows, uint[][] cols, int expectedDifferences)
        {
            var sum = 0;
            for (int n = 0; n < rows.Length; ++n)
            {
                var row = rows[n];
                var col = cols[n];
                var differences = int.MaxValue;
                sum += 100 * ProcessRowOrCol(row, expectedDifferences, ref differences);
                sum +=       ProcessRowOrCol(col, expectedDifferences, ref differences);
            }
            return sum;
        }

        private static int ProcessRowOrCol(uint[] data, int expectedDifferences, ref int differences)
        {
            for (int n = 1; n < data.Length && differences != expectedDifferences; ++n)
            {
                differences = 0;
                for (int low = n - 1, hi = n; low >= 0 && hi < data.Length && differences <= expectedDifferences; --low, ++hi)
                {
                    differences += BitOperations.PopCount(data[low] ^ data[hi]);
                }
                if (differences == expectedDifferences)
                {
                    return n;
                }
            }
            return 0;
        }

        private static uint[] ProcessRows(string[] map)
        {
            return map.Select(ProcessRow).ToArray();
        }

        private static uint ProcessRow(string row)
        {
            return row.Select((c, n) => c == '#' ? One << n : Zero).Aggregate((x, y) => x | y);
        }

        private static uint[] ProcessColumns(string[] map)
        {
            return Enumerable.Range(0, map[0].Length).Select(col => ProcessColumn(col, map)).ToArray();
        }

        private static uint ProcessColumn(int col, string[] map)
        {
            return ProcessRow(string.Concat(map.Select(row => row[col])));
        }

        private static string[] SplitLines(string line)
        {
            return line.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
