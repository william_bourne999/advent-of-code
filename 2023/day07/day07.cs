﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Concurrent;
    using System.IO;
    using System.Linq;

    public static class Day07
    {
        private enum Card { Joker = 1, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace }
        private enum Type { HighCard, OnePair, TwoPair, ThreeOfAKind, FullHouse, FourOfAKind, FiveOfAKind }
        private sealed record Hand(Type Type, Card[] Cards, int Bid) : IComparable<Hand>
        {
            public int CompareTo(Hand other) => CompareTuple().CompareTo(other.CompareTuple());
            public (Type, Card, Card, Card, Card, Card) CompareTuple() => (Type, Cards[0], Cards[1], Cards[2], Cards[3], Cards[4]);
        }

        public static void Main()
        {
            // For Part One, sum the winnings of each hand using the Jack card:
            var input = File.ReadAllLines("2023/day07/input");
            Console.WriteLine("Part One = {0}", GetSumOfWinnings(input, false));

            // For Part Two, the Jack cards become Jokers, and can act as any other card to make the best hand:
            Console.WriteLine("Part Two = {0}", GetSumOfWinnings(input, true));
        }

        private static int GetSumOfWinnings(string[] input, bool joker)
        {
            // Parse the hands from the input and sort them based on their type and card values:
            var hands = input.Select(str => ParseHand(str, joker)).ToList();
            hands.Sort();

            // Sum the winnings of each hand, which is the bid multiplied by the rank:
            return hands.Select((hand, rank) => hand.Bid * (rank + 1)).Sum();
        }

        private static Hand ParseHand(string str, bool joker)
        {
            var parts = str.Split(' ');
            var cards = parts[0].Select(c => ParseCard(c, joker)).ToArray();
            return new Hand(GetType(cards), cards, int.Parse(parts[1]));
        }

        private static Card ParseCard(char c, bool joker)
        {
            return c switch
            {
                'T' => Card.Ten,
                'J' => joker ? Card.Joker : Card.Jack,
                'Q' => Card.Queen,
                'K' => Card.King,
                'A' => Card.Ace,
                _ => (Card)(c - '0')
            };
        }

        private static Type GetType(Card[] cards)
        {
            var cardMap = new ConcurrentDictionary<Card, int>();
            Array.ForEach(cards, card => cardMap.AddOrUpdate(card, 1, (c, x) => x + 1));

            // If we have any joker cards, and other types of card, then use those jokers to make the best hand:
            if (cardMap.Count > 1 && cardMap.TryRemove(Card.Joker, out var jokers))
            {
                var max = cardMap.Values.Max();
                var best = cardMap.Where(x => x.Value == max).MaxBy(x => x.Key);
                cardMap.TryUpdate(best.Key, best.Value + jokers, best.Value);
            }

            var counts = cardMap.Values.Order().ToList();
            if (counts.Count == 1) return Type.FiveOfAKind;
            if (counts.Count == 2) return counts[1] == 4 ? Type.FourOfAKind : Type.FullHouse;
            if (counts.Count == 3) return counts[2] == 3 ? Type.ThreeOfAKind : Type.TwoPair;
            return counts.Count == 4 ? Type.OnePair : Type.HighCard;
        }
    }
}
