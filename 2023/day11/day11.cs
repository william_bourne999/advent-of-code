﻿namespace AdventOfCode2023
{
    using System;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day11
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2023/day11/input");
            var size = new Vec2D(input[0].Length, input.Length);
            var points = Enumerable.Range(0, size.X * size.Y).Select(n => new Vec2D(n % size.Y, n / size.Y)).ToArray();
            var galaxies = points.Where(x => input[x.Y][x.X] == '#').ToArray();
            var emptyRows = Enumerable.Range(0, size.Y).Where(y => input[y].All(c => c == '.')).ToArray();
            var emptyCols = Enumerable.Range(0, size.X).Where(x => input.All(s => s[x] == '.')).ToArray();

            // In Parts One and Two, find and sum all the shortest distances between the galaxies with different expansion rates:
            Console.WriteLine("Part One = {0}", SumMinimumDistances(galaxies, emptyRows, emptyCols, 2));
            Console.WriteLine("Part Two = {0}", SumMinimumDistances(galaxies, emptyRows, emptyCols, 1000000));
        }

        private static ulong SumMinimumDistances(Vec2D[] galaxies, int[] emptyRows, int[] emptyCols, int expansion)
        {
            var distances = ulong.MinValue;
            for (var i = 0; i < galaxies.Length; ++i)
            {
                for (var j = i + 1; j < galaxies.Length; ++j)
                {
                    var start    = galaxies[i];
                    var end      = galaxies[j];
                    var offset   = new Vec2D(end.X - start.X, end.Y - start.Y);
                    var emptyRow = Enumerable.Range(Math.Min(start.Y, end.Y), Math.Abs(offset.Y)).Count(emptyRows.Contains);
                    var emptyCol = Enumerable.Range(Math.Min(start.X, end.X), Math.Abs(offset.X)).Count(emptyCols.Contains);
                    var distance = Math.Abs(offset.X) + Math.Abs(offset.Y) + (expansion - 1) * (emptyRow + emptyCol);
                    distances += Convert.ToUInt64(distance);
                }
            }
            return distances;
        }
    }
}
