﻿namespace AdventOfCode2023
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day06
    {
        public static void Main()
        {
            var input     = File.ReadAllLines("2023/day06/input");
            var times     = input[0].Substring(10).Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(ulong.Parse).ToList();
            var distances = input[1].Substring(10).Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(ulong.Parse).ToList();

            // In Part One, each millisecond the button is held adds one millimeter per second of speed.
            // Find the number of ways to win each race and multiply them:
            var partOne = (ulong)1;
            for (var n = 0; n < times.Count; n++) partOne *= GetSolutions(times[n], distances[n]);
            Console.WriteLine("Part One = {0}", partOne);

            // In Part Two, there is only one single race which has all the numbers from the input concatenated:
            var partTwo = GetSolutions(ulong.Parse(string.Concat(times)), ulong.Parse(string.Concat(distances)));
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static ulong GetSolutions(ulong time, ulong distance)
        {
            // N ms of button holding results in N mm/s for (T - N) ms, which travels N * (T - N) mm
            // Find all N such that N * (T - N) > D => - N² + T*N - D > 0
            // Using the quadratic formula with a = -1, b = T and c = -D gives us:
            // N = T ± √ T² - 4*D / 2, assuming T² - 4*D is positive
            // This gives us two solutions, and we count the number of integers strictly between them:
            var determinant = (time * time) - 4 * distance;
            if (determinant >= 0)
            {
                var min = 0.5 * (time - Math.Sqrt(determinant));
                var max = 0.5 * (time + Math.Sqrt(determinant));
                return (ulong)Math.Ceiling(max) - (ulong)Math.Floor(min) - 1;
            }
            return 1;
        }
    }
}
