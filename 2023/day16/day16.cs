﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day16
    {
        private sealed record PathPos(Vec2D Pos, Vec2D Dir);

        public static void Main()
        {
            var tiles = File.ReadAllLines("2023/day16/input");
            var size = new Vec2D(tiles[0].Length, tiles.Length);

            // Trace the light paths for each starting location:
            var energized = new Dictionary<Vec2D, int>();
            for (var x = 0; x < size.X; ++x)
            {
                var upPos   = new Vec2D(x, size.Y);
                var downPos = new Vec2D(x, -1);
                energized.Add(upPos,   TraceLightPath(tiles, size, upPos,   Vec2D.Up  ));
                energized.Add(downPos, TraceLightPath(tiles, size, downPos, Vec2D.Down));
            }
            for (var y = 0; y < size.Y; ++y)
            {
                var leftPos  = new Vec2D(size.X, y);
                var rightPos = new Vec2D(-1, y);
                energized.Add(leftPos,  TraceLightPath(tiles, size, leftPos,  Vec2D.Left ));
                energized.Add(rightPos, TraceLightPath(tiles, size, rightPos, Vec2D.Right));
            }

            // In Part One, return the number of energized tiles when starting in the top left and going right:
            Console.WriteLine("Part One = {0}", energized[new(-1, 0)]);

            // In Part Two, return the maximum number of energized tiles from any starting position:
            Console.WriteLine("Part Two = {0}", energized.Values.Max());
        }

        private static int TraceLightPath(string[] tiles, Vec2D size, Vec2D startPos, Vec2D startDir)
        {
            var visited = new HashSet<PathPos>();
            var queue   = new Queue<PathPos>();
            queue.Enqueue(new PathPos(startPos, startDir));
            while (queue.TryDequeue(out var prev))
            {
                var dir = prev.Dir;
                var pos = prev.Pos + prev.Dir;
                if (pos.X < 0 || pos.X >= size.X || pos.Y < 0 || pos.Y >= size.Y) continue;

                var tile = tiles[pos.Y][pos.X];
                if (tile == '.' || (tile == '-' && dir.Y == 0) || (tile == '|' && dir.X == 0))
                {
                    var next = new PathPos(pos, dir);
                    if (visited.Add(next)) queue.Enqueue(next);
                }
                else if (tile == '/')
                {
                    var next = new PathPos(pos, new(-dir.Y, -dir.X));
                    if (visited.Add(next)) queue.Enqueue(next);
                }
                else if (tile == '\\')
                {
                    var next = new PathPos(pos, new(dir.Y, dir.X));
                    if (visited.Add(next)) queue.Enqueue(next);
                }
                else if (tile == '-')
                {
                    var left  = new PathPos(pos, Vec2D.Left);
                    var right = new PathPos(pos, Vec2D.Right);
                    if (visited.Add(left )) queue.Enqueue(left);
                    if (visited.Add(right)) queue.Enqueue(right);
                }
                else if (tile == '|')
                {
                    var up   = new PathPos(pos, Vec2D.Up);
                    var down = new PathPos(pos, Vec2D.Down);
                    if (visited.Add(up  )) queue.Enqueue(up);
                    if (visited.Add(down)) queue.Enqueue(down);
                }
            }
            return visited.DistinctBy(x => x.Pos).Count();
        }
    }
}
