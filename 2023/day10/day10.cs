﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day10
    {
        private enum Pipe { None, Horizontal, Vertical, BottomToLeft, BottomToRight, TopToLeft, TopToRight, Start }
        private enum Direction { None, Up, Down, Left, Right }
        private sealed record PipePoint(Vec2D Pos, Pipe Pipe, Direction Dir);

        private static readonly Pipe[] CrossingPipes = new[] { Pipe.Vertical, Pipe.TopToLeft, Pipe.TopToRight };
        private static readonly Direction[] Dirs = new[] { Direction.Left, Direction.Right, Direction.Up, Direction.Down };
        private static readonly Vec2D[] DirVecs = new Vec2D[] { new(0, 0), new(0, -1), new(0, 1), new(-1, 0), new(1, 0) };

        public static void Main()
        {
            var input  = File.ReadAllLines("2023/day10/input");
            var pipes  = input.Select(x => x.Select(ParsePipe).ToArray()).ToArray();
            var size   = new Vec2D(pipes[0].Length, pipes.Length);
            var points = Enumerable.Range(0, size.X * size.Y).Select(n => new Vec2D(n / size.Y, n % size.Y)).ToArray();
            var start  = points.FirstOrDefault(pos => TryGetNeighbour(pos, Direction.None, size, pipes)?.Pipe == Pipe.Start);

            // From the start we don't know which direction to head, so look in the four possible directions:
            var possibles = Dirs.Select(dir => TryGetNeighbour(start, dir, size, pipes)).ToArray();
            var positions = possibles.Where(x => NextDirection(x.Dir, x.Pipe) != Direction.None).ToArray();
            var directions = positions.Select(x => x.Dir).Order().ToArray();

            // Replace the start node with the correct pipe type:
            var startPipe = Pipe.None;
                 if (directions[0] == Direction.Up   && directions[1] == Direction.Down ) startPipe = Pipe.Vertical;
            else if (directions[0] == Direction.Up   && directions[1] == Direction.Left ) startPipe = Pipe.TopToLeft;
            else if (directions[0] == Direction.Up   && directions[1] == Direction.Right) startPipe = Pipe.TopToRight;
            else if (directions[0] == Direction.Down && directions[1] == Direction.Left ) startPipe = Pipe.BottomToLeft;
            else if (directions[0] == Direction.Down && directions[1] == Direction.Right) startPipe = Pipe.BottomToRight;
            else if (directions[0] == Direction.Left && directions[1] == Direction.Right) startPipe = Pipe.Horizontal;
            pipes[start.Y][start.X] = startPipe;

            // Follow the loop around in that direction:
            var position = positions[0];
            var loop = new List<PipePoint> { position };
            while (position.Pos != start)
            {
                position = TryGetNeighbour(position.Pos, NextDirection(position.Dir, position.Pipe), size, pipes);
                loop.Add(position);
            }
            Console.WriteLine("Part One = {0}", loop.Count / 2);

            // Loop through each line and keep track of the crossing points to determine which points are inside the loop:
            var insidePoints = 0;
            for (var y = 0; y < size.Y; ++y)
            {
                var inside = false;
                for (var x = 0; x < size.X; ++x)
                {
                    var pos = new Vec2D(x, y);
                    var loopPos = loop.FirstOrDefault(p => p.Pos == pos);
                    if (loopPos != null)
                    {
                        if (CrossingPipes.Contains(loopPos.Pipe)) inside = !inside;
                    }
                    else if (inside) ++insidePoints;
                }
            }
            Console.WriteLine("Part Two = {0}", insidePoints);
        }

        private static PipePoint TryGetNeighbour(Vec2D pos, Direction dir, Vec2D size, Pipe[][] pipes)
        {
            var point = pos + DirVecs[(int)dir];
            if (point.X < 0 || point.X >= size.X || point.Y < 0 || point.Y >= size.Y) return null;
            return new(point, pipes[point.Y][point.X], dir);
        }

        private static Direction NextDirection(Direction dir, Pipe pipe)
        {
            return pipe switch
            {
                Pipe.Horizontal    => dir == Direction.Left || dir == Direction.Right ? dir : Direction.None,
                Pipe.Vertical      => dir == Direction.Up   || dir == Direction.Down  ? dir : Direction.None,
                Pipe.BottomToLeft  => dir == Direction.Up   ? Direction.Left  : dir == Direction.Right ? Direction.Down : Direction.None,
                Pipe.BottomToRight => dir == Direction.Up   ? Direction.Right : dir == Direction.Left  ? Direction.Down : Direction.None,
                Pipe.TopToLeft     => dir == Direction.Down ? Direction.Left  : dir == Direction.Right ? Direction.Up   : Direction.None,
                Pipe.TopToRight    => dir == Direction.Down ? Direction.Right : dir == Direction.Left  ? Direction.Up   : Direction.None,
                _ => Direction.None,
            };
        }

        private static Pipe ParsePipe(char c)
        {
            return c switch
            {
                '-' => Pipe.Horizontal,
                '|' => Pipe.Vertical,
                '7' => Pipe.BottomToLeft,
                'F' => Pipe.BottomToRight,
                'J' => Pipe.TopToLeft,
                'L' => Pipe.TopToRight,
                'S' => Pipe.Start,
                _ => Pipe.None,
            };
        }
    }
}
