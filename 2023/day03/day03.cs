namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day03
    {
        private const char Gear = '*';
        private const string Symbols = "+-=*#@$�%^&*/\\";

        public static void Main()
        {
            var input = File.ReadAllLines("2023/day03/input");

            // Loop over the the engine schematic and find the numbers that are surrounded by symbols:
            var partOne = 0;
            var gears = new Dictionary<Vec2D, List<int>>();
            for (var y = 0; y < input.Length; y++)
            {
                var line = input[y];
                for (var x = 0; x < line.Length; ++x)
                {
                    var number = string.Concat(line.Skip(x).TakeWhile(char.IsNumber));
                    if (int.TryParse(number, out var value))
                    {
                        var foundSymbol = false;
                        var startY = Math.Max(0, y - 1);
                        var startX = Math.Max(0, x - 1);
                        var endY = Math.Min(y + 1, input.Length - 1);
                        var endX = Math.Min(x + number.Length, input.Length - 1);
                        for (var searchY = startY; searchY <= endY; ++searchY)
                        {
                            for (var searchX = startX; searchX <= endX; ++searchX)
                            {
                                var c = input[searchY][searchX];
                                foundSymbol |= Symbols.Contains(c);
                                if (c == Gear) gears.GetOrAdd(new(searchX, searchY)).Add(value);
                            }
                        }

                        x += number.Length - 1;
                        partOne += foundSymbol ? value : 0;
                    }
                }
            }

            // In Part One, we sum all of the values that are surrounded by symbols:
            Console.WriteLine("Part One = {0}", partOne);

            // In Part Two, we must sum all of the gear ratios from all the gears:
            var partTwo = gears.Where(x => x.Value.Count == 2).Sum(x => x.Value[0] * x.Value[1]);
            Console.WriteLine("Part Two = {0}", partTwo);
        }
    }
}
