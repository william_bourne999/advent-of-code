namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day05
    {
        private sealed record Seeds(ulong Min, ulong Max);
        private sealed record Range(ulong Destination, ulong Source, ulong Count);
        private sealed record SeedMap(List<ulong> Seeds, List<List<Range>> Maps);

        public static void Main()
        {
            var input = File.ReadAllText("2023/day05/input").ReplaceLineEndings();

            // In Part One, map all the seeds to their locations and find the lowest location:
            var map = ParseSeedMap(input);
            var partOne = map.Seeds.Min(seed => map.Maps.Aggregate(seed, MapValue));
            Console.WriteLine("Part One = {0}", partOne);

            // Part Two - Reverse the mappings and search for the first location which maps to a seed:
            var reverseMap = map.Maps.Select(list => list.Select(ReverseRange).ToList()).ToList();
            reverseMap.Reverse();

            var partTwo = ulong.MaxValue;
            var indices = Enumerable.Range(0, map.Seeds.Count / 2).Select(x => 2 * x);
            var seeds = indices.Select(i => new Seeds(map.Seeds[i], map.Seeds[i] + map.Seeds[i + 1])).ToList();
            for (var location = ulong.MinValue; location < ulong.MaxValue; ++location)
            {
                var seed = reverseMap.Aggregate(location, MapValue);
                if (seeds.Any(range => seed >= range.Min && seed < range.Max))
                {
                    partTwo = location;
                    break;
                }
            }
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static SeedMap ParseSeedMap(string str)
        {
            var sections = str.Split(Environment.NewLine + Environment.NewLine);
            var seeds    = ParseValues(sections[0].Substring(7));
            var maps     = sections.Skip(1).Select(ParseRanges).ToList();
            return new SeedMap(seeds, maps);
        }

        private static List<Range> ParseRanges(string str)
        {
            return str.Split(Environment.NewLine).Skip(1).Select(ParseRange).ToList();
        }

        private static Range ParseRange(string str)
        {
            var values = ParseValues(str);
            return new Range(values[0], values[1], values[2]);
        }

        private static List<ulong> ParseValues(string str)
        {
            return str.Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(ulong.Parse).ToList();
        }

        private static Range ReverseRange(Range range)
        {
            return new Range(range.Source, range.Destination, range.Count);
        }

        private static ulong MapValue(ulong value, List<Range> ranges)
        {
            foreach (var range in ranges)
            {
                var offset = value - range.Source;
                if (offset >= 0 && offset < range.Count) return range.Destination + offset;
            }
            return value;
        }
    }
}
