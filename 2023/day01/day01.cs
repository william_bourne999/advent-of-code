namespace AdventOfCode2023
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day01
    {
        private static string[] _numbers = new[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

        public static void Main()
        {
            var input = File.ReadAllLines("2023/day01/input");

            // Part One - Sum the first and last digits as a single two digit number from each line:
            var partOne = input.Sum(SumPartOne);
            Console.WriteLine("Part One = {0}", partOne);

            // Part Two - Sum of first and last digit or number word:
            var partTwo = input.Sum(SumPartTwo);
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static int SumPartOne(string str)
        {
            return 10 * (str.FirstOrDefault(char.IsDigit) - '0') + (str.LastOrDefault(char.IsDigit) - '0');
        }

        private static int SumPartTwo(string str)
        {
            var range = Enumerable.Range(0, str.Length);
            var first = range.Select(n => FindDigit(str, n)).FirstOrDefault(x => x > 0);
            var last  = range.Reverse().Select(n => FindDigit(str, n)).FirstOrDefault(x => x > 0);
            return (10 * first) + last;
        }

        private static int FindDigit(string str, int n)
        {
            return char.IsDigit(str[n]) ? str[n] - '0' : Array.FindIndex(_numbers, str.Substring(n).StartsWith) + 1;
        }
    }
}
