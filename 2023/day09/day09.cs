﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day09
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2023/day09/input");
            var values = input.Select(x => x.Split(' ').Select(int.Parse).ToList()).ToList();

            // In Part One, find the differences between each element until all differences are zero, then use this
            // to predict the next element in the original list of values. Then return the sum of all new values.
            // In Part Two, we do the same but extrapolating backwards from the start of the differences.
            // We can do them both in one loop over the input data:
            var differences = new List<List<int>>();
            var predictions = new List<(int, int)>(values.Count);
            for (var i = 0; i < values.Count; i++)
            {
                var last = values[i];
                differences.Add(last);
                while (last.Any(x => x != 0))
                {
                    last = last.Zip(last.Skip(1), (x, y) => y - x).ToList();
                    differences.Add(last);
                }

                for (var n = differences.Count - 1; n > 0; --n)
                {
                    var next = differences[n - 1];
                    var current = differences[n];
                    next.Add(next.Last() + current.Last());
                    next.Insert(0, next.First() - current.First());
                }

                var extrapolated = differences.First();
                predictions.Add((extrapolated.Last(), extrapolated.First()));
                differences.Clear();
            }
            Console.WriteLine("Part One = {0}", predictions.Sum(x => x.Item1));
            Console.WriteLine("Part Two = {0}", predictions.Sum(x => x.Item2));
        }
    }
}
