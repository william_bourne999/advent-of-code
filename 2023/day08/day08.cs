﻿namespace AdventOfCode2023
{
    using System;
    using System.IO;
    using System.Linq;

    using AdventOfCode.Utils;

    public static class Day08
    {
        private sealed record Node(string Label, string Left, string Right);

        public static void Main()
        {
            var input = File.ReadAllLines("2023/day08/input");
            var instructions = input[0].Select(c => c == 'L' ? 0 : 1).ToArray();
            var nodes = input.Skip(2).Select(ParseNode).ToArray();
            int nodeIndex(string label) => Array.FindIndex(nodes, n => n.Label == label);

            // In Part One, follow the instructions from AAA to ZZZ and count the number of steps:
            var curLocation = nodeIndex("AAA");
            var endLocation = nodeIndex("ZZZ");
            var indices = nodes.Select(node => new[] { nodeIndex(node.Left), nodeIndex(node.Right) }).ToArray();
            for (var step = 0; step < int.MaxValue; ++step)
            {
                var instructionIndex = step % instructions.Length;
                var direction = instructions[instructionIndex];
                curLocation = indices[curLocation][direction];

                if (curLocation == endLocation)
                {
                    Console.WriteLine("Part One = {0}", step + 1);
                    break;
                }
            }

            // In Part Two, follow the instructions from **A to **Z and count the number of steps for each starting location to loop:
            var locations    = Enumerable.Range(0, nodes.Length).Where(n => nodes[n].Label.EndsWith('A')).ToArray();
            var endLocations = Enumerable.Range(0, nodes.Length).Where(n => nodes[n].Label.EndsWith('Z')).ToArray();
            var loopingPeriods = new long[locations.Length];
            for (var step = 0; step < int.MaxValue; ++step)
            {
                var instructionIndex = step % instructions.Length;
                var direction = instructions[instructionIndex];
                for (var n = 0; n < locations.Length; ++n)
                {
                    ref int location = ref locations[n];
                    location = indices[location][direction];

                    var endLocationIndex = Array.IndexOf(endLocations, location);
                    if (endLocationIndex != -1)
                    {
                        ref long period = ref loopingPeriods[n];
                             if (period == 0) period -= step;
                        else if (period  < 0) period += step;

                    }
                }
                if (loopingPeriods.All(x => x > 0)) break;
            }

            // Find the lowest common multiple of all the looping periods:
            Console.WriteLine("Part Two = {0}", loopingPeriods.Aggregate(MathEx.LeastCommonMultiple));
        }

        private static Node ParseNode(string str)
        {
            var parts = str.Split(" = ");
            var directions = parts[1].Trim('(', ')').Split(", ");
            return new Node(parts[0], directions[0], directions[1]);
        }
    }
}
