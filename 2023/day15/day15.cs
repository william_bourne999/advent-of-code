﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day15
    {
        private sealed record Instruction(string Label, int Box, int FocalLength);
        private static readonly char[] SplitChars = new[] { '=', '-' };

        public static void Main()
        {
            var input = File.ReadAllText("2023/day15/input");
            var sequence = input.Split(',', StringSplitOptions.RemoveEmptyEntries);
            var instructions = sequence.Select(ParseInstruction).ToArray();

            // In Part One, just sum the hash values of each step in the initialization sequence:
            Console.WriteLine("Part One = {0}", sequence.Sum(HashString));

            // In Part Two, follow the instructions by adding a removing the lens to each box:
            var boxes = Enumerable.Range(0, 256).Select(x => new List<Instruction>()).ToArray();
            foreach (var instruction in instructions)
            {
                var box = boxes[instruction.Box];
                var index = box.FindIndex(x => x.Label == instruction.Label);
                if (instruction.FocalLength < 0)
                {
                    if (index != -1) box.RemoveAt(index);
                }
                else
                {
                    if (index != -1) box[index] = instruction; 
                    else             box.Add(instruction);
                }
            }

            // Then sum all of the focusing powers of the lenses in each box:
            var focusingPower = 0;
            for (var i = 0; i < boxes.Length; ++i)
            {
                var box = boxes[i];
                for (var j = 0; j < box.Count; ++j) focusingPower += (i + 1) * (j + 1) * box[j].FocalLength;
            }
            Console.WriteLine("Part Two = {0}", focusingPower);
        }

        private static Instruction ParseInstruction(string str)
        {
            var parts = str.Split(SplitChars, StringSplitOptions.RemoveEmptyEntries);
            return new Instruction(parts[0], HashString(parts[0]), parts.Length > 1 ? int.Parse(parts[1]) : -1);
        }

        private static int HashString(string str)
        {
            var value = 0;
            foreach (var c in str)
            {
                value += c;
                value *= 17;
                value %= 256;
            }
            return value;
        }
    }
}
