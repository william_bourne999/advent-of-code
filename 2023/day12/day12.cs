﻿namespace AdventOfCode2023
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Linq;

    public static class Day12
    {
        private enum State { Unknown, Damaged, Operational }
        private sealed record Spring(State[] States, int[] Groups);

        private const long Zero = 0;
        private const long One = 1;

        public static void Main()
        {
            // ???.### 1,1,3
            // .??..??...?##. 1,1,3
            // ?#?#?#?#?#?#?#? 1,3,1,6
            // ????.#...#... 4,1,1
            // ????.######..#####. 1,6,5
            // ?###???????? 3,2,1

            var input = File.ReadAllLines("2023/day12/input");
            var springs = input.Select(ParseSpring).ToArray();

            // In Part One, sum all the different possible arrangements for each row:
            Console.WriteLine("BRUTEFORCE --- Part One = {0}", springs.Sum(CountPossibleArrangements    )); // 8419 for real, 21 for example
            Console.WriteLine("\r\nTESTING    --- Part One = {0}", springs.Sum(DeterminePossibleArrangements)); // 8419 for real, 21 for example

            // In Part Two, unfold spring and then count the combinations again:
            //var unfolded = springs.Select(UnfoldSpring).ToArray();
            //Console.WriteLine("Part Two = {0}", unfolded.Sum(CountPossibleArrangements)); // 8419 for real, 21 for example
        }

        private static long CountPossibleArrangements(Spring spring)
        {
            // Find all the unknowns then find each combination of possible arrangements:
            var arrangements = Zero;
            var unknowns = spring.States.Select((s, n) => (s, n)).Where(x => x.s == State.Unknown).ToArray();
            var combinations = One << unknowns.Length;
            var states = spring.States.ToArray();
            for (var n = Zero; n < combinations; ++n) // TODO - This sucks for larger unknown counts...
            {
                for (var i = 0; i < unknowns.Length; ++i)
                {
                    var unknown = unknowns[i];
                    states[unknown.n] = (n & (One << i)) != 0 ? State.Operational : State.Damaged;
                }
                arrangements += IsCorrectGroups(states, spring.Groups) ? One : Zero;
            }

            Console.WriteLine("States: {0} Arrangements: {1}", string.Concat(spring.States.Select(DisplayState)), arrangements);
            return arrangements;
        }

        private static long DeterminePossibleArrangements(Spring spring)
        {
            //var currentGroupSize = 0;
            //var previousState = State.Unknown;
            //var groupIndex = 0;
            var sections = new List<List<State>>();
            var currentSection = new List<State>();
            for (var n = 0; n < spring.States.Length; ++n)
            {
                var state = spring.States[n];
                if (state == State.Operational)
                {
                    if (currentSection.Count > 0)
                    {
                        sections.Add(currentSection);
                        currentSection = new List<State>();
                    }
                }
                else if (state == State.Damaged)
                {
                    currentSection.Add(State.Damaged);
                }
                else if (state == State.Unknown)
                {
                    // Can't tell what needs to go here, but we should be able to tell what it is by looking at the group counts.
                    currentSection.Add(State.Unknown);
                }
            }
            if (currentSection.Count > 0) sections.Add(currentSection);

            // Each section can correspond to any number of groups in the group list:
            Console.WriteLine();
            Console.WriteLine("States: {0}", string.Concat(spring.States.Select(DisplayState)));
            Console.WriteLine("Groups: {0}", string.Join(", ", spring.Groups));
            foreach (var section in sections)
            {
                Console.WriteLine(string.Join(", ", section.Select(DisplayState)));
            }

            var remainingSections = sections.ToList();
            var remainingGroups = spring.Groups.ToList();
            var arrangements = Zero;
            var currentSize = remainingSections.Count + remainingGroups.Count;
            var previousSize = 0;
            while (currentSize > 0 && currentSize != previousSize)
            {
                previousSize = currentSize;
                ProcessRemainingSectionsAndGroups(remainingGroups, remainingSections, ref arrangements);
                currentSize = remainingSections.Count + remainingGroups.Count;
            }

            arrangements = arrangements == Zero ? One : arrangements;
            Console.WriteLine(" --- Returning Arrangements: {0}", arrangements);
            return arrangements;
        }

        private static void ProcessRemainingSectionsAndGroups(List<int> remainingGroups, List<List<State>> remainingSections, ref long arrangements)
        {
            var groupIndex = 0;
            for (var n = 0; n < remainingSections.Count; ++n)
            {
                // Simplest case, section is all Damaged and is the same length as the current group:
                var section = remainingSections[n];
                var group = remainingGroups[groupIndex];
                var display = string.Concat(section.Select(DisplayState));
                if (section.All(x => x == State.Damaged) && section.Count == group)
                {
                    // No arrangements to add here, just increment the group counter:
                    Console.WriteLine("Section {0} is exactly Group {1}", display, group);
                    remainingSections.RemoveAt(n--);
                    remainingGroups.RemoveAt(groupIndex);
                }

                // Next simplest case, the section is all Unknown, we need to match against as many groups as we can:
                // Not all combinations will correspond to the same amount of groups...
                else if (section.All(x => x == State.Unknown))
                {
                    // If this section doesn't fit in the group then we're doomed!
                    if (section.Count < group)
                    {
                        Console.WriteLine("DOOMED !!! Section {0} is too small to fit in Group {1}", display, group);
                    }

                    // If the current group is the same length then there's only one arrangement:
                    else if (section.Count == group)
                    {
                        Console.WriteLine("Section {0} is exactly Group {1}", display, group);
                        remainingSections.RemoveAt(n--);
                        remainingGroups.RemoveAt(groupIndex);
                    }

                    // If we have one more state than the current group size, either of the states can be Damaged:
                    else if (section.Count == group + 1)
                    {
                        Console.WriteLine("Section {0} has one more than Group {1} - Adding {2} arrangements", display, group, section.Count);
                        remainingSections.RemoveAt(n--);
                        remainingGroups.RemoveAt(groupIndex);
                        arrangements += section.Count;
                    }

                    // The ??? => 1,1 case:
                    else if (section.Count == 3 && group == 1 && groupIndex < remainingGroups.Count - 1 && remainingGroups[groupIndex + 1] == 1)
                    {
                        Console.WriteLine("Section {0} can map ony one way to Groups {1} and {2}", display, 1, 1);
                        remainingSections.RemoveAt(n--);
                        remainingGroups.RemoveAt(groupIndex);
                        remainingGroups.RemoveAt(groupIndex);
                    }

                    // The ?*** to last 1 case:
                    else if (group == 1 && remainingGroups.Count == 1 && remainingSections.Count == 1)
                    {
                        Console.WriteLine("Section {0} is the last section and last group - Adding {1} arrangements", display, section.Count);
                        remainingSections.RemoveAt(n--);
                        remainingGroups.RemoveAt(groupIndex);
                        arrangements += section.Count;
                    }

                    // The ?*** to last groups case:
                    else if (remainingSections.Count == 1)
                    {
                        var unknownCount = section.Count;
                        var groups = remainingGroups; // How many ways can we arrange unknownCount into thsoe groups...
                        var totalGroupSize = remainingGroups.Sum(); // Each group takes up group + 1 space, except last ???
                        var triangleNumber = Enumerable.Range(1, Math.Max(1,  unknownCount - totalGroupSize)).Sum();
                        Console.WriteLine(" - TESTING Section {0} fitting groups {1} - Triangle Number: {2}", display, string.Join(", ", groups), triangleNumber);

                        remainingSections.Clear();
                        remainingGroups.Clear();
                        arrangements += triangleNumber;
                    }

                    // Catch for now
                    else
                    {
                        Console.WriteLine("FIXME !!! No handler for Section {0} and Group {1}", display, group);
                        //remainingSections.RemoveAt(n--);
                        //remainingGroups.RemoveAt(groupIndex);
                        ++groupIndex; // Just skip the current group and see what happens:
                    }
                }

                // Otherwise we have a mixture of unknowns and damaged:
                else
                {
                    // If the section size matches the group size then there's only one arrangement:
                    if (section.Count == group)
                    {
                        Console.WriteLine("Section {0} can only map one way to Group {1}", display, group);
                        remainingSections.RemoveAt(n--);
                        remainingGroups.RemoveAt(groupIndex);
                    }

                    else
                    {
                        // TODO - These could be more generic:
                        var count = section.Count;
                        if (group == 1 && count > 2 && section[0] == State.Unknown && section[1] == State.Damaged && section[2] == State.Unknown)
                        {
                            Console.WriteLine("Section {0} Index 0 can only start with .#.");
                            section.RemoveRange(0, 3);
                            remainingGroups.RemoveAt(groupIndex);
                        }

                        else if (group == 1 && count > 1 && section[0] == State.Damaged && section[1] == State.Unknown)
                        {
                            Console.WriteLine("Section {0} Index 0 can only start with #.");
                            section.RemoveRange(0, 2);
                            remainingGroups.RemoveAt(groupIndex);
                        }

                        else if (count > group + 1 && section[0] == State.Unknown && section[group + 1] == State.Unknown && Enumerable.Range(1, group).All(n => section[n] == State.Damaged)) // TODO - Only good at end
                        {
                            Console.WriteLine("Section {0} Index 0 can only start with .{1}.", display, new string('#', group));
                            section.RemoveRange(0, group + 2);
                            remainingGroups.RemoveAt(groupIndex);
                        }

                        // Catch for now
                        else
                        {
                            Console.WriteLine("FIXME !!! No handler for Section {0} and Group {1}", display, group);
                            //remainingSections.RemoveAt(n--);
                            //remainingGroups.RemoveAt(groupIndex);
                            ++groupIndex; // Just skip the current group and see what happens:
                        }
                    }
                }
            }

            if (remainingSections.Count > 0) Console.WriteLine("Remaining Sections:");
            foreach (var section in remainingSections)
            {
                Console.WriteLine(string.Join(", ", section.Select(DisplayState)));
            }
            if (remainingGroups.Count > 0) Console.WriteLine("Remaining Groups: {0}", string.Join(", ", remainingGroups));
        }

        private static bool IsCorrectGroups(State[] states, int[] Groups)
        {
            var groups = new List<int>();
            var currentGroup = 0;
            foreach (var state in states)
            {
                if (state == State.Damaged) ++currentGroup;
                else if (currentGroup > 0) { groups.Add(currentGroup); currentGroup = 0; }
            }
            if (currentGroup > 0) groups.Add(currentGroup);
            return groups.SequenceEqual(Groups);
        }

        private static Spring UnfoldSpring(Spring spring)
        {
            var states = new List<State>();
            var groups = new List<int>();
            for (var n = 0; n < 5; ++n)
            {
                states.AddRange(spring.States);
                groups.AddRange(spring.Groups);
                if (n < 4) states.Add(State.Unknown);
            }
            return new Spring(states.ToArray(), groups.ToArray());
        }

        private static Spring ParseSpring(string str)
        {
            var parts = str.Split(' ');
            var states = parts[0].Select(ParseState).ToArray();
            var groups = parts[1].Split(',').Select(int.Parse).ToArray();
            return new Spring(states, groups);
        }

        private static State ParseState(char c)
        {
            return c switch
            {
                '#' => State.Damaged,
                '.' => State.Operational,
                _ => State.Unknown,
            };
        }

        private static char DisplayState(State state)
        {
            return state switch
            {
                State.Damaged => '#',
                State.Operational => '.',
                _ => '?'
            };
        }
    }
}
