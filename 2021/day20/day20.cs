﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day20
    {
        public record Vector(int X, int Y);
        public record Image(HashSet<Vector> Lit) { public bool Outside { get; set; } }

        public static void Main()
        {
            var input  = File.ReadAllLines("2021/day20/input");
            var lookup = input[0].Select(IsLitPixel).ToArray();
            var pixels = input.Skip(2).SelectMany((s, y) => s.SelectMany((t, x) => Yield(new Vector(x, y), IsLitPixel(t))));
            var image  = new Image(new HashSet<Vector>(pixels));

            // Part One - How many lit pixels are there after 2 enhancements:
            for (int n = 0; n < 2; ++n) EnhanceImage(image, lookup);
            Console.WriteLine("Part One = {0}", image.Lit.Count);

            // Part Two - How many lit pixels are there after 50 enhancements:
            for (int n = 2; n < 50; ++n) EnhanceImage(image, lookup);
            Console.WriteLine("Part One = {0}", image.Lit.Count);
        }

        private static bool IsLitPixel(char c) => c == '#';
        private static IEnumerable<T> Yield<T>(T value, bool cond) { if (cond) yield return value; }

        private static void EnhanceImage(Image image, bool[] lookup)
        {
            var points = new HashSet<Vector>();
            int minX = image.Lit.Min(x => x.X), minY = image.Lit.Min(x => x.Y);
            int maxX = image.Lit.Max(x => x.X), maxY = image.Lit.Max(x => x.Y);
            for (int y = minY - 1; y <= maxY + 1; ++y)
            {
                for (int x = minX - 1; x <= maxX + 1; ++x)
                {
                    var index = 0;
                    for (int i = 0; i < 9; ++i)
                    {
                        int posX = x - 1 + (i % 3), posY = y - 1 + (i / 3);
                        var outside = posX < minX || posX > maxX || posY < minY || posY > maxY;
                        var lit = outside ? image.Outside : image.Lit.Contains(new(posX, posY));
                        index |= (lit ? 1 : 0) << (8 - i);
                    }

                    if (lookup[index]) points.Add(new(x, y));
                }
            }

            image.Lit.Clear();
            image.Lit.UnionWith(points);
            image.Outside = lookup[image.Outside ? lookup.Length - 1 : 0];
        }
    }
}
