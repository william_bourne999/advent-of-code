﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day19
    {
        private static readonly Vector[] Orientations = new Vector[]
        {
            new(1, 2, 3), new(-1, 2, 3), new(1, -2, 3), new(1, 2, -3), new(1, -2, -3), new(-1, 2, -3), new(-1, -2, 3), new(-1, -2, -3),
            new(1, 3, 2), new(-1, 3, 2), new(1, -3, 2), new(1, 3, -2), new(1, -3, -2), new(-1, 3, -2), new(-1, -3, 2), new(-1, -3, -2),
            new(2, 1, 3), new(-2, 1, 3), new(2, -1, 3), new(2, 1, -3), new(2, -1, -3), new(-2, 1, -3), new(-2, -1, 3), new(-2, -1, -3),
            new(2, 3, 1), new(-2, 3, 1), new(2, -3, 1), new(2, 3, -1), new(2, -3, -1), new(-2, 3, -1), new(-2, -3, 1), new(-2, -3, -1),
            new(3, 1, 2), new(-3, 1, 2), new(3, -1, 2), new(3, 1, -2), new(3, -1, -2), new(-3, 1, -2), new(-3, -1, 2), new(-3, -1, -2),
            new(3, 2, 1), new(-3, 2, 1), new(3, -2, 1), new(3, 2, -1), new(3, -2, -1), new(-3, 2, -1), new(-3, -2, 1), new(-3, -2, -1),
        };

        public static void Main()
        {
            var input = File.ReadAllLines("2021/day19/input");
            var scanners = new List<Scanner>();
            for (int n = 0; n < input.Length; ++n)
                scanners.Add(new(input, ref n, scanners.Count));

            // Part One - Find how many beacons there are:
            var scannerList  = new List<Vector>(new[] { new Vector(0, 0, 0) });
            var beaconMap    = new HashSet<Vector>(scanners[0].Beacons);
            var pendingList  = scanners.Skip(1).ToList();
            var orientations = pendingList.ToDictionary(s => s, s => Orientations.Select(o => new Scanner(s, o)).ToList());
            while (pendingList.Count > 0) FindAlignment(pendingList, scannerList, beaconMap, orientations);
            Console.WriteLine("Part One = {0}", beaconMap.Count);

            // Part Two - What is the largest Manhattan distance between the scanners:
            var distance = 0;
            for (int i = 0; i < scannerList.Count; ++i)
            {
                for (int j = 0; j < i; ++j)
                {
                    var vec = scannerList[i] - scannerList[j];
                    var dist = Math.Abs(vec.X) + Math.Abs(vec.Y) + Math.Abs(vec.Z);
                    distance = Math.Max(distance, dist);
                }
            }
            Console.WriteLine("Part Two = {0}", distance);
        }

        private static void FindAlignment(
            List<Scanner> pendingList,
            List<Vector> scannerList,
            HashSet<Vector> beaconMap,
            Dictionary<Scanner, List<Scanner>> orientations)
        {
            foreach (var pending in pendingList)
            {
                foreach (var oriented in orientations[pending])
                {
                    foreach (var orientedOrigin in oriented.Beacons)
                    {
                        foreach (var beaconOrigin in beaconMap)
                        {
                            var found = 0;
                            var offset = beaconOrigin - orientedOrigin;
                            foreach (var item in oriented.Beacons)
                            {
                                var vector = item + offset;
                                if (beaconMap.Contains(vector))
                                {
                                    found++;
                                }
                                if (found >= 12)
                                {
                                    scannerList.Add(offset);
                                    foreach (var beacon in oriented.Beacons) beaconMap.Add(beacon + offset);
                                    pendingList.Remove(pending);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        private record Vector(int X, int Y, int Z)
        {
            public static Vector operator +(Vector left, Vector right)
                => new(left.X + right.X, left.Y + right.Y, left.Z + right.Z);

            public static Vector operator -(Vector left, Vector right)
                => new(left.X - right.X, left.Y - right.Y, left.Z - right.Z);

            public static Vector operator -(Vector left) => new(-left.X, -left.Y, -left.Z);

            public int this[int n] => n switch { 1 => X, -1 => -X, 2 => Y, -2 => -Y, 3 => Z,-3 => -Z, _ => 0 };
            public Vector this[Vector n] => new(this[n.X], this[n.Y], this[n.Z]);
        }

        private class Scanner
        {
            public Scanner(string[] input, ref int n, int id)
            {
                for (; n < input.Length; ++n)
                {
                    var str = input[n];
                    if (str.Length == 0) break;
                    if (str.Contains("scanner")) continue;

                    var values = str.Split(',').Select(int.Parse).ToArray();
                    if (values.Length == 2) Beacons.Add(new Vector(values[0], values[1], 0));
                    else Beacons.Add(new Vector(values[0], values[1], values[2]));
                }
            }

            public Scanner(Scanner scanner, Vector orient)
            {
                Beacons.AddRange(scanner.Beacons.Select(x => x[orient]));
            }

            public List<Vector> Beacons { get; } = new();
        }
    }
}
