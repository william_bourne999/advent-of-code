﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day10
    {
        private const string Openings = "([{<";
        private const string Closings = ")]}>";
        private static readonly int[] Scores = new[] { 3, 57, 1197, 25137 };

        public static void Main()
        {
            var input = File.ReadAllLines("2021/day10/input");

            // Part One - Find all corrupted lines and sum their scores:
            var partOne = 0;
            var incomplete = new List<long>();
            foreach (var line in input)
            {
                var stack = new Stack<char>();
                foreach (var c in line)
                {
                    // Check if this is an opening token:
                    var open = Openings.Contains(c);
                    if (open) { stack.Push(c); continue; }

                    // Otherwise it must be a closing token:
                    var close = Closings.IndexOf(c);
                    if (stack.Count == 0 || Openings.IndexOf(stack.Peek()) != close)
                    {
                        // This line is corrupt, so add to score for this token:
                        partOne += Scores[close];
                        stack.Clear();
                        break;
                    }
                    else
                    {
                        // Otherwise it does match, so remove the item from stack:
                        stack.Pop();
                    }
                }

                // Part Two - Find the score to complete the incomplete lines:
                if (stack.Count > 0)
                {
                    long score = 0;
                    foreach (var token in stack) score = (5 * score) + Openings.IndexOf(token) + 1;
                    incomplete.Add(score);
                }
            }
            Console.WriteLine("Part One = {0}", partOne);

            // Part Two - Take the score of the middle incomplete line:
            var partTwo = incomplete.OrderBy(x => x).ElementAt(incomplete.Count / 2);
            Console.WriteLine("Part Two = {0}", partTwo);
        }
    }
}
