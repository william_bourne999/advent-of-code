﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day23
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day23/input");
            var rooms = new char[8];
            var corridor = ".._._._._..".ToCharArray();
            for (int x = 3, n = 0; x < 10; x += 2) for (int y = 2; y < 4; ++y) rooms[n++] = input[y][x];

            // Part One - Find the least energy required to organise the amphipods:
            Console.WriteLine("Part One = {0}", TrySolve(new State(corridor, rooms)));

            // Part Two - The rooms have two extra spaces in each:
            var burrows = new char[16];
            var extra = "  #D#C#B#A#  |  #D#B#A#C#  ".Split('|');
            for (int x = 3, n = 0; x < 10; x += 2)
            {
                burrows[n++] = input[2][x];
                burrows[n++] = extra[0][x];
                burrows[n++] = extra[1][x];
                burrows[n++] = input[3][x];
            }
            Console.WriteLine("Part Two = {0}", TrySolve(new State(corridor, burrows)));
        }

        private static int? TrySolve(State state)
        {
            int score = 0;
            while (MakeMove(state, ref score)) { }
            return state.IsComplete() ? score : null;
        }

        private static bool MakeMove(State state, ref int score)
        {
            // If the state is complete then we do not need to do anything:
            if (state.IsComplete()) return false;

            // Consider each place in each room in turn:
            var height = state.Rooms.Length / 4;
            for (int room = 0; room < 4; ++room)
            {
                for (int pos = 0; pos < height - 1; ++pos)
                {
                    // Move down to bottom room position if free and we are in the right room:
                    var home = (char)('A' + room);
                    var topN = (height * room) + pos;
                    var botN = topN + 1;
                    var topC = state.Rooms[topN];
                    var botC = state.Rooms[botN];
                    var correct = Enumerable.Range(topN, height - pos).All(x => state.Rooms[x] == home || state.Rooms[x] == '.');
                    if (topC == home && botC == '.' && topC != '.' && correct)
                    {
                        state.Rooms[botN] = topC;
                        state.Rooms[topN] = '.';
                        score += MoveCost(topC, 1);
                        return true;
                    }

                    // Move up to top room position if free and we are in the wrong room:
                    if (topC == '.' && botC != '.' && !correct)
                    {
                        state.Rooms[topN] = botC;
                        state.Rooms[botN] = '.';
                        score += MoveCost(botC, 1);
                        return true;
                    }

                    // Move to the top of the correct room if the corridor and target room is free:
                    if (pos == 0 && char.IsLetter(topC))
                    {
                        var homeRoom = topC - 'A';
                        var homeTopN = height * homeRoom;
                        var corrThis = 2 + (room * 2);
                        var corrThat = 2 + (homeRoom * 2);
                        var corrMin  = Math.Min(corrThis, corrThat);
                        var corrLen  = Math.Max(corrThis, corrThat) - corrMin + 1;
                        var corrFree = state.Corridor.Skip(corrMin).Take(corrLen).All(x => x == '.' || x == '_');
                        var homeFree = Enumerable.Range(homeTopN + 1, height - 1).All(x => state.Rooms[x] == '.' || state.Rooms[x] == topC);
                        if (topC != home && state.Rooms[homeTopN] == '.' && homeFree && corrFree)
                        {
                            state.Rooms[homeTopN] = topC;
                            state.Rooms[topN] = '.';
                            score += MoveCost(topC, corrLen + 1);
                            return true;
                        }
                    }
                }
            }

            // Try to move any pods in the corridor into their rooms:
            for (int corrThis = 0; corrThis < state.Corridor.Length; ++corrThis)
            {
                var corrC = state.Corridor[corrThis];
                if (!char.IsLetter(corrC)) continue;

                var homeRoom = corrC - 'A';
                var homeTopN = height * homeRoom;
                var corrThat = 2 + (homeRoom * 2);
                var corrDiff = Math.Sign(corrThat - corrThis);
                var corrMin = Math.Min(corrThis + corrDiff, corrThat);
                var corrLen = Math.Max(corrThis + corrDiff, corrThat) - corrMin + 1;
                var corrFree = state.Corridor.Skip(corrMin).Take(corrLen).All(x => x == '.' || x == '_');
                var homeFree = Enumerable.Range(homeTopN + 1, height - 1).All(x => state.Rooms[x] == '.' || state.Rooms[x] == corrC);
                if (state.Rooms[homeTopN] == '.' && homeFree && corrFree)
                {
                    state.Rooms[homeTopN] = corrC;
                    state.Corridor[corrThis] = '.';
                    score += MoveCost(corrC, corrLen + 1);
                    return true;
                }
            }

            // Now we must try every possible combination of moves to find the best one:
            State beststate = null;
            var bestscore = int.MaxValue;
            for (int room = 0; room < 4; ++room)
            {
                var home = (char)('A' + room);
                var topN = height * room;
                var topC = state.Rooms[topN];
                var correct = Enumerable.Range(topN, height).All(x => state.Rooms[x] == home);
                if (char.IsLetter(topC) && !correct)
                {
                    // Get valid places to leave this pod in the corridor:
                    var corrThis = 2 + (room * 2);
                    var validPositions = state.GetCorridorSpaces(corrThis);

                    // Create a copy of the state and go from there:
                    foreach (var corrPos in validPositions)
                    {
                        var newstate = new State(state);
                        var newscore = score;

                        newstate.Rooms[topN] = '.';
                        newstate.Corridor[corrPos] = topC;
                        newscore += MoveCost(topC, 1 + Math.Abs(corrThis - corrPos));

                        var solution = TrySolve(newstate) + newscore;
                        if (solution.HasValue && solution.Value < bestscore)
                        {
                            bestscore = solution.Value;
                            beststate = newstate;
                        }
                    }
                }
            }

            // We found a best guess so take it:
            if (bestscore < int.MaxValue)
            {
                score = bestscore;
                state.Copy(beststate);
            }

            // No more moves to take:
            return false;
        }

        private static int MoveCost(char type, int steps)
        {
            return (int)Math.Pow(10, type - 'A') * steps;
        }

        private record State(char[] Corridor, char[] Rooms)
        {
            public State(State other)
            {
                Corridor = other.Corridor.ToArray();
                Rooms    = other.Rooms   .ToArray();
            }

            public void Copy(State other)
            {
                Array.Copy(other.Corridor, Corridor, Corridor.Length);
                Array.Copy(other.Rooms,    Rooms,    Rooms   .Length);
            }

            public List<int> GetCorridorSpaces(int start)
            {
                var validPositions = new List<int>();
                for (int i = start - 1; i >= 0; --i)
                {
                    if (Corridor[i] == '_') continue;
                    if (Corridor[i] == '.') validPositions.Add(i);
                    else break;
                }
                for (int j = start + 1; j < Corridor.Length; ++j)
                {
                    if (Corridor[j] == '_') continue;
                    if (Corridor[j] == '.') validPositions.Add(j);
                    else break;
                }
                return validPositions;
            }

            public bool IsComplete()
            {
                var corridor = Corridor.All(x => x == '.' || x == '_');
                var rooms = Rooms.Zip(Rooms.Skip(1), (a, b) => a <= b).All(x => x);
                return corridor && rooms;
            }
        }
    }
}
