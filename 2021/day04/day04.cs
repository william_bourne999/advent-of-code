﻿namespace AdventOfCode2021
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day04
    {
        public static void Main()
        {
            var text   = File.ReadAllText("2021/day04/input").Replace(Environment.NewLine, "\n");
            var parts  = text.Split("\n\n", StringSplitOptions.RemoveEmptyEntries);
            var values = parts[0].Split(',').Select(int.Parse).ToArray();
            var boards = parts.Skip(1).Select(str => new BingoBoard(str)).ToArray();

            // Part One - Find which board will win first and get its score:
            // Part Two - Find which board will win  last and get its score:
            foreach (var value in values)
            {
                foreach (var board in boards)
                {
                    int score = board.AddNumber(value);
                    if (score > 0) Console.WriteLine("Score = {0}", score);
                }
            }
        }

        private class BingoBoard
        {
            public readonly int[] Board = new int[25];

            public BingoBoard(string str)
            {
                Board = str.Split(new[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            }

            public int AddNumber(int value)
            {
                if (HasWon()) return 0;
                for (int n = 0; n < 25; ++n)
                {
                    if (Board[n] == value)
                    {
                        Board[n] = 0;
                        if (HasWon()) return value * Board.Sum();
                    }
                }
                return 0;
            }

            public bool HasWon()
            {
                return
                    Enumerable.Range(0, 5).Any(x => Enumerable.Range(0, 5).All(y => Board[x + (5*y)] == 0)) ||
                    Enumerable.Range(0, 5).Any(y => Enumerable.Range(0, 5).All(x => Board[x + (5*y)] == 0));
            }
        }
    }
}
