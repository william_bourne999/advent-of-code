﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day12
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day12/input");
            var connections = input.Select(str => new Connection(str)).ToArray();

            // Part One - Find all paths through the caves visiting small caves only once:
            var partOne = CheckCaves(connections, false);
            Console.WriteLine("Part One = {0}", partOne);

            // Part Two - Now a single small cave may be visited twice:
            var partTwo = CheckCaves(connections, true);
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static int CheckCaves(Connection[] connections, bool repeat, Path path = null, HashSet < Path> paths = null)
        {
            path ??= new Path();
            paths ??= new HashSet<Path>();
            foreach (var connection in connections)
            {
                for (int n = 0; n < 2; ++n)
                {
                    var from = connection.Caves[n];
                    if (!path.EndsWith(from)) continue;

                    var to      = connection.Caves[1 - n];
                    var isbig   = to.All(char.IsUpper);
                    var visited = path.CountOf(to);
                    var allow   = isbig || visited == 0;

                    // Check if we allow a single small cave to be repeated:
                    if (repeat && !allow && visited == 1 && to != "start") allow |= path.NoRepeats();

                    // if this connection is allowed then add it to the current path:
                    if (allow)
                    {
                        // If this connects to the end then we are done:
                        if (to == "end") paths.Add(path.Concat("end"));

                        // Otherwise add to our path and continue:
                        else CheckCaves(connections, repeat, path.Concat(to), paths);
                    }
                }
            }

            // Return the number of unique paths:
            return paths.Count;
        }

        private class Connection
        {
            public string[] Caves { get; }
            public string Start => Caves[0];
            public string End   => Caves[1];
            public Connection(string str) { Caves = str.Split('-'); }
            public override string ToString() => string.Join("-", Caves);
        }

        private class Path
        {
            private List<string> List { get; set; } = new List<string> { "start" };

            public int  CountOf (string str) => List.Count(x => x == str);
            public bool EndsWith(string str) => List.Last() == str;
            public Path Concat  (string str) => new Path { List = List.Concat(new[] { str }).ToList() };
            public bool NoRepeats() => List.Where(x => x.All(char.IsLower)).GroupBy(x => x).All(x => x.Count() == 1);

            public override string ToString() => string.Join(",", List);
            public override int    GetHashCode() => List.GetHashCode();
            public override bool   Equals(object obj) => obj is Path path && List.SequenceEqual(path.List);
        }
    }
}
