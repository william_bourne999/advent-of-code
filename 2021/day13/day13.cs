﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day13
    {
        private record Fold(bool InX, int N);
        private record Vector(int X, int Y);

        public static void Main()
        {
            var input  = File.ReadAllLines("2021/day13/input");
            var index  = Array.IndexOf(input, string.Empty);
            var points = new HashSet<Vector>(input.Take(index).Select(ParseVector));
            var folds  = input.Skip(index + 1).Select(ParseFold).ToArray();

            // Part One - How many dots are visible after completing the first fold:
            points = Process(points, folds[0]);
            Console.WriteLine("Part One = {0}", points.Count);

            // Part Two - Find the hidden code in the dots after all the folds:
            foreach (var fold in folds.Skip(1)) points = Process(points, fold);
            var result = string.Concat(Enumerable.Range(0, 8).Select(x => GetChar(x * 5, points)));
            Console.WriteLine("Part Two = {0}", result);
        }

        private static Vector ParseVector(string str)
        {
            var values = str.Split(',');
            return new(int.Parse(values[0]), int.Parse(values[1]));
        }

        private static Fold ParseFold(string str)
        {
            var index = str.IndexOf('=');
            return new(str[index - 1] == 'x', int.Parse(str.Substring(index + 1)));
        }

        private static HashSet<Vector> Process(HashSet<Vector> points, Fold fold)
        {
            var max = 2 * fold.N;
            var output = new HashSet<Vector>();
            foreach (var point in points)
            {
                var x =  fold.InX && point.X > fold.N ? max - point.X : point.X;
                var y = !fold.InX && point.Y > fold.N ? max - point.Y : point.Y;
                output.Add(new(x, y));
            }
            return output;
        }

        private static string GetChar(int X, HashSet<Vector> points)
        {
            var lines = Enumerable.Range(0, 6).Select(y => GetLine(X, y, points)).ToArray();
            return lines[0] switch
            {
                "####" => lines[2] == "###." ? lines[5] == "####" ? "E" : "F" : "Z",
                ".##." => lines[5] == "#..#" ? "A" : "G",
                "#..#" => lines[5] == ".##." ? "U" : "H",
                "###." => "R",
                _ => string.Empty,
            };
        }

        private static string GetLine(int X, int y, HashSet<Vector> points)
        {
            return string.Concat(Enumerable.Range(X, 4).Select(x => points.Contains(new(x, y)) ? '#' : '.'));
        }
    }
}
