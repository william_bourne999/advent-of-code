﻿namespace AdventOfCode2021
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day06
    {
        public static void Main()
        {
            var fish = new long[10];
            var calc = new long[10];
            var input = File.ReadAllText("2021/day06/input").Split(',').Select(long.Parse).ToList();
            foreach (var age in input) fish[age]++;

            // Part One - Simulate the lanternfish for 80 days:
            // Part Two - Simulate the lanternfish for 256 days:
            for (int day = 0; day <= 256; ++day)
            {
                if (day ==  80) Console.WriteLine("Part One = {0}", fish.Sum());
                if (day == 256) Console.WriteLine("Part Two = {0}", fish.Sum());

                Array.Fill(calc, 0);
                for (int age = 0; age < 10; ++age)
                {
                    if (age == 0) calc[8] += calc[6] += fish[age];
                    else          calc[age - 1]      += fish[age];
                }
                Array.Copy(calc, fish, 10);
            }
        }
    }
}
