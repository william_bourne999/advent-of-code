﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day21
    {
        public static void Main()
        {
            var input   = File.ReadAllLines("2021/day21/input");
            var starts  = input.Select(x => int.Parse(x[(x.LastIndexOf(' ') + 1)..])).ToArray();
            var initial = new State(starts[0], 0, starts[1], 0);

            // Part One - Calculate (MinScore * Rolls) with deterministic dice:
            var board = initial;
            int die = 0, rolls = 0;
            for (bool one = true; board.Score1 < 1000 && board.Score2 < 1000; one = !one)
            {
                var pos = one ? board.Pos1 : board.Pos2;
                for (int n = 0; n < 3; ++n, ++rolls) pos = AddPosition(pos, die = (die % 100) + 1);
                var score = (one ? board.Score1 : board.Score2) + pos;
                board = new State(one, pos, score, board);
            }
            Console.WriteLine("Part One = {0}", Math.Min(board.Score1, board.Score2) * rolls);

            // Part Two - Find the number of successes for the best player across all universes:
            var wins   = new long[2];
            var calcs  = new Dictionary<State, long>();
            var states = new Dictionary<State, long> { { initial, 1 } };
            for (bool one =  true; states.Count > 0; one = !one)
            {
                calcs.Clear();
                foreach (var (state, count) in states)
                {
                    for (int dieA = 3; dieA > 0; --dieA) for (int dieB = 3; dieB > 0; --dieB) for (int dieC = 3; dieC > 0; --dieC)
                    {
                        var pos = AddPosition(one ? state.Pos1 : state.Pos2, dieA + dieB + dieC);
                        var score = (one ? state.Score1 : state.Score2) + pos;
                        if (score > 20)
                        {
                            wins[one ? 0 : 1] += count;
                        }
                        else
                        {
                            var newstate = new State(one, pos, score, state);
                            if (calcs.ContainsKey(newstate)) calcs[newstate] += count;
                            else calcs.Add(newstate, count);
                        }
                    }
                }

                var temp = states;
                states = calcs;
                calcs = temp;
            }
            Console.WriteLine("Part Two = {0}", wins.Max());
        }

        private static int AddPosition(int position, int steps)
        {
            return ((position + steps - 1) % 10) + 1;
        }

        private record State(int Pos1, int Score1, int Pos2, int Score2)
        {
            public State(bool one, int pos, int score, State other) : this(0, 0, 0, 0)
            {
                Pos1   = one ? pos   : other.Pos1;
                Score1 = one ? score : other.Score1;
                Pos2   = one ? other.Pos2   : pos;
                Score2 = one ? other.Score2 : score;
            }
        }
    }
}
