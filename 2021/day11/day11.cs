﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day11
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day11/input");
            var array = input.Select(x => x.Select(x => x - '0').ToArray()).ToArray();
       
            var partOne = 0;
            var partTwo = 0;
            var flashers = new HashSet<(int, int)>();
            for (int step = 1; step < int.MaxValue; ++step)
            {
                // Increment each value by one:
                For2D(0, 10, 0, 10, (x, y) => array[y][x]++);

                // Any value larger than nine will affect its neighbours:
                flashers.Clear();
                var prevCount = -1;
                while (flashers.Count != prevCount)
                {
                    prevCount = flashers.Count;
                    For2D(0, 10, 0, 10, (x, y) =>
                    {
                        if (!flashers.Contains((x, y)) && array[y][x] > 9)
                        {
                            flashers.Add((x, y));
                            int minX = Math.Max(x - 1, 0), maxX = Math.Min(x + 2, 10);
                            int minY = Math.Max(y - 1, 0), maxY = Math.Min(y + 2, 10);
                            For2D(minX, maxX, minY, maxY, (X, Y) => array[Y][X]++);
                        }
                    });
                }

                // Reset the flashing values:
                foreach (var (x, y) in flashers) array[y][x] = 0;

                // Part One - Process 100 steps and count all flashes:
                if (step <= 100) partOne += flashers.Count;

                // Part Two - Find the first step where all octopi flash:
                if (flashers.Count == 100) { partTwo = step; break; }
            }
            Console.WriteLine("Part One = {0}", partOne);
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static void For2D(int minX, int maxX, int minY, int maxY, Action<int, int> action)
        {
            for (int x = minX; x < maxX; ++x) for (int y = minY; y < maxY; ++y) action(x, y);
        }
    }
}
