﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day15
    {
        private record Vector(int X, int Y);

        public static void Main()
        {
            var input = File.ReadAllLines("2021/day15/input");

            // Part One - Search for the path with the lowest score:
            var partOne = Dijkstra(input, 1);
            Console.WriteLine("Part One = {0}", partOne);

            // Part Two - The map is actually five times larger than the input:
            var partTwo = Dijkstra(input, 5);
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static int Dijkstra(string[] input, int repeat)
        {
            // Create the array of distances:
            var maxX   = input[0].Length;
            var maxY   = input   .Length;
            var points = new HashSet<Vector>();
            var dist   = new Dictionary<Vector, int>();
            var queue  = new SortedDictionary<int, Stack<Vector>>();
            var final  = new Vector(maxX * repeat - 1, maxY * repeat - 1);
            for (int x = 0; x <= final.X; ++x)
            {
                for (int y = 0; y <= final.Y; ++y)
                {
                    var pos = new Vector(x, y);
                    points.Add(pos);
                    dist.Add(pos, int.MaxValue);
                }
            }

            // Set the source distance to zero:
            dist[new(0, 0)] = 0;
            QueuePush(queue, 0, new(0, 0));

            // Loop over all points to find their distances:
            while (points.Any())
            {
                var u = QueuePop(queue);
                points.Remove(u);

                for (int n = 0; n < 4; ++n)
                {
                    var x = u.X + (n == 0 ? -1 : n == 1 ? 1 : 0);
                    var y = u.Y + (n == 2 ? -1 : n == 3 ? 1 : 0);
                    var v = new Vector(x, y);
                    if (points.Contains(v))
                    {
                        var extra = (v.X / maxX) + (v.Y / maxY);
                        var value = input[v.Y % maxY][v.X % maxX] - '0';
                        var exact = ((value + extra - 1) % 9) + 1;
                        var distance = dist[u] + exact;
                        if (distance < dist[v])
                        {
                            dist[v] = distance;
                            QueuePush(queue, distance, v);
                        }
                    }
                }
            }

            // Return the distance to the final square:
            return dist[final];
        }

        private static void QueuePush(SortedDictionary<int, Stack<Vector>> queue, int key, Vector value)
        {
            if (!queue.ContainsKey(key)) queue.Add(key, new Stack<Vector>());
            queue[key].Push(value);
        }

        private static Vector QueuePop(SortedDictionary<int, Stack<Vector>> queue)
        {
            if (queue.Count == 0) return default;
            var (key, stack) = queue.First();
            var value = stack.Pop();
            if (stack.Count == 0) queue.Remove(key);
            return value;
        }
    }
}
