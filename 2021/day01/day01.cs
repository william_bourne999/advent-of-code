namespace AdventOfCode2021
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day01
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day01/input").Select(int.Parse).ToList();

            // Part One - Find how many measurements are greater than the previous measurement:
            var greaterOnes = input.Zip(input.Skip(1), (a, b) => b > a ? 1 : 0).Sum();
            Console.WriteLine("Part One = {0}", greaterOnes);

            // Part Two - Find how many groups of three measurements are greater than the previous sum of three:
            var sumsOfThree = Enumerable.Range(0, input.Count - 2).Select(n => input.Skip(n).Take(3).Sum()).ToList();
            var greaterThree = sumsOfThree.Zip(sumsOfThree.Skip(1), (a, b) => b > a ? 1 : 0).Sum();
            Console.WriteLine("Part One = {0}", greaterThree);
        }
    }
}
