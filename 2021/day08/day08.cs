﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Intrinsics.X86;

    public static class Day08
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day08/input").Select(x => x.Split(' ')).ToList();
            var partOne = 0;
            var partTwo = 0;

            foreach (var entry in input)
            {
                // Find digits 1,4,7,8 first since they have a unique segment count:
                var numbers  = new uint[10];
                var indicies = new List<int>();
                var entries  = entry.Select(x => x.Aggregate(0, (res, c) => res | (1 << (c - 'a')))).Select(Convert.ToUInt32).ToArray();
                for (int n = 0; n < 10; ++n)
                {
                    var number = entries[n];
                    switch (Popcnt.PopCount(number))
                    {
                        case 2: numbers[1] = number; break;
                        case 4: numbers[4] = number; break;
                        case 3: numbers[7] = number; break;
                        case 7: numbers[8] = number; break;
                        default: indicies.Add(n); break;
                    }
                }

                // Part One - Find how many 1,4,7,8 digits appear in the output values:
                for (int n = 11; n < entries.Length; ++n) partOne += Array.IndexOf(numbers, entries[n]) != -1 ? 1 : 0;

                // Now find the other numbers by determining their uniqueness against the known digits:
                foreach (var n in indicies)
                {
                    var number = entries[n];
                    var popN   = Popcnt.PopCount(number);
                    var pop7   = Popcnt.PopCount(number & ~numbers[7]);
                    var big    = Popcnt.PopCount(number & ~numbers[4]) == 2;
                    var index  = pop7 == 2 ? 3 : pop7 == 4 ? 6 : popN == 5 ? big ? 5 : 2 : big ? 9 : 0;
                    numbers[index] = number;
                }

                // Part Two - Find the sum of all the output values:
                var output = 0;
                for (int n = 11; n < entries.Length; ++n) output = 10*output + Array.IndexOf(numbers, entries[n]);
                partTwo += output;
            }
            Console.WriteLine("Part One = {0}", partOne);
            Console.WriteLine("Part Two = {0}", partTwo);
        }
    }
}
