﻿namespace AdventOfCode2021
{
    using System;
    using System.IO;

    public static class Day17
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day17/input")[0].Split('=', '.', ',');
            int xmin  = int.Parse(input[1]), xmax = int.Parse(input[3]);
            int ymin  = int.Parse(input[5]), ymax = int.Parse(input[7]);
            var vxmin = (int)Math.Floor((-1 + Math.Sqrt(1 + 8 * xmin)) / 2) - 1;

            int partOne = 0, partTwo = 0;
            for (int vx = vxmin; vx <= xmax; ++vx)
            {
                for (int vy = ymin; vy <= 1000; ++vy)
                {
                    for (int x = 0, y = 0, dx = vx, dy = vy, maxy = 0; y >= ymin && x <= xmax;)
                    {
                        x  += dx;
                        y  += dy;
                        dy -= 1;
                        dx -= Math.Sign(dx);
                        maxy = Math.Max(maxy, y);

                        if (x >= xmin && x <= xmax && y >= ymin && y <= ymax)
                        {
                            partOne = Math.Max(partOne, maxy); // Part One - Find the highest Y value from any velocity:
                            partTwo += 1;                      // Part Two - Find how many velocities can enter the target zone:
                            break;
                        }
                    }
                }
            }
            Console.WriteLine("Part One = {0}", partOne);
            Console.WriteLine("Part Two = {0}", partTwo);
        }
    }
}
