﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;

    public static class Day03
    {
        public static void Main()
        {
            var lines = File.ReadAllLines("2021/day03/input");
            var input = lines.Select(x => new BitVector32(Convert.ToInt32(x, 2))).ToList();

            // Part One - Find the most common bit of each binary number:
            var max    = 1 << (lines[0].Length - 1);
            var gamma  = new BitVector32(0);
            for (var n = max; n > 0; n >>= 1) gamma[n] = OneMostCommonBit(input, n);
            Console.WriteLine("Part One = {0}", gamma.Data * ((~gamma.Data) & (max - 1)));

            // Part Two - Use the criteria to find the two ratings:
            var  O2Input = input.ToList();
            var CO2Input = input.ToList();
            var   oxygen = new BitVector32(0);
            var scrubber = new BitVector32(0);
            for (var n = max; n > 0; n >>= 1)
            {
                if (O2Input.Count > 1)
                {
                    var one = OneMostCommonBit(O2Input, n);
                    O2Input.RemoveAll(x => x[n] == one);
                    if (O2Input.Count == 1) oxygen = O2Input[0];
                }

                if (CO2Input.Count > 1)
                {
                    var one = OneMostCommonBit(CO2Input, n);
                    CO2Input.RemoveAll(x => x[n] != one);
                    if (CO2Input.Count == 1) scrubber = CO2Input[0];
                }
            }
            Console.WriteLine("Part Two = {0}", oxygen.Data * scrubber.Data);
        }

        private static bool OneMostCommonBit(IReadOnlyCollection<BitVector32> input, int n)
        {
            return 2 * input.Count(x => x[n]) >= input.Count;
        }
    }
}
