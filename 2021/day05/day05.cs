﻿namespace AdventOfCode2021
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day05
    {
        public static void Main()
        {
            var partOne = new int[1000, 1000];
            var partTwo = new int[1000, 1000];
            var lines = File.ReadAllLines("2021/day05/input").Select(x => new Line(x)).ToArray();

            foreach (var line in lines)
            {
                for (int x = line.Start.X, y = line.Start.Y; ; x += line.DX, y += line.DY)
                {
                    // Part One - Only consider horizontal and vertical lines:
                    if (line.DX == 0 || line.DY == 0) partOne[x, y]++;

                    // Part Two - Consider horizontal, vertical and diagonal lines:
                    partTwo[x, y]++;

                    // Break when we reach the end of the line:
                    if (x == line.End.X && y == line.End.Y) break;
                }
            }

            Console.WriteLine("Part One = {0}", partOne.OfType<int>().Count(x => x > 1));
            Console.WriteLine("Part Two = {0}", partTwo.OfType<int>().Count(x => x > 1));
        }

        private class Line
        {
            public (int X, int Y) Start { get; }
            public (int X, int Y) End   { get; }
            public int DX => Start.X < End.X ? 1 : Start.X > End.X ? -1 : 0;
            public int DY => Start.Y < End.Y ? 1 : Start.Y > End.Y ? -1 : 0;

            public Line(string str)
            {
                var values = str.Split(" -> ").SelectMany(x => x.Split(',')).Select(int.Parse).ToList();
                Start = (values[0], values[1]);
                End   = (values[2], values[3]);
            }
        }
    }
}
