﻿namespace AdventOfCode2021
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day07
    {
        public static void Main()
        {
            var input = File.ReadAllText("2021/day07/input").Split(',').Select(int.Parse).ToList();

            // Loop through all target values and find the least expensive position to move to:
            int max = input.Max(), partOne = int.MaxValue, partTwo = int.MaxValue;
            for (int target = 0; target <= max; ++target)
            {
                int fuelOne = 0, fuelTwo = 0;
                foreach (var value in input)
                {
                    // Part One - Each position costs one fuel to move to:
                    var dist = Math.Abs(value - target);
                    fuelOne += dist;

                    // Part Two - Each position costs the triangular sum to move to:
                    fuelTwo += dist * (dist + 1) / 2;
                }
                partOne = Math.Min(fuelOne, partOne);
                partTwo = Math.Min(fuelTwo, partTwo);
            }
            Console.WriteLine("Part One = {0}", partOne);
            Console.WriteLine("Part Two = {0}", partTwo);
        }
    }
}
