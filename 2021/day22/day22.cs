﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day22
    {
        private record Vector(long X, long Y, long Z);
        private record Cube(Vector Min, Vector Max);
        private record Command(bool On, Cube Cube);

        public static void Main()
        {
            var input = File.ReadAllLines("2021/day22/input").Select(ParseCommand).ToArray();
            var inits = input.Where(IsInitializeRegion).ToArray();
            var other = input.Except(inits).ToArray();

            // Part One - How many cubes are on in the initialization range:
            var state = new List<Command>();
            foreach (var inst in inits) Process(inst, state);
            Console.WriteLine("Part One = {0}", state.Sum(CubeCount));

            // Part Two - How many cubes are on in the the entire range:
            foreach (var inst in other) Process(inst, state);
            Console.WriteLine("Part Two = {0}", state.Sum(CubeCount));
        }

        private static void Process(Command command, List<Command> state)
        {
            // Find overlapping regions with the current state:
            var overlaps = new List<Command>();
            foreach (var region in state)
            {
                var minX = Math.Max(command.Cube.Min.X, region.Cube.Min.X);
                var minY = Math.Max(command.Cube.Min.Y, region.Cube.Min.Y);
                var minZ = Math.Max(command.Cube.Min.Z, region.Cube.Min.Z);
                var maxX = Math.Min(command.Cube.Max.X, region.Cube.Max.X);
                var maxY = Math.Min(command.Cube.Max.Y, region.Cube.Max.Y);
                var maxZ = Math.Min(command.Cube.Max.Z, region.Cube.Max.Z);
                if (minX <= maxX && minY <= maxY && minZ <= maxZ)
                {
                    overlaps.Add(new(!region.On, new(new(minX, minY, minZ), new(maxX, maxY, maxZ))));
                }
            }

            // Add this region and overlaps to the state:
            if (command.On) overlaps.Add(command);
            state.AddRange(overlaps);
        }

        private static Command ParseCommand(string str)
        {
            var s = "onf =,.xyz".ToCharArray();
            var v = str.Split(s, StringSplitOptions.RemoveEmptyEntries).Select(long.Parse).ToArray();
            return new(str.StartsWith("on"), new(new(v[0], v[2], v[4]), new(v[1], v[3], v[5])));
        }

        private static bool IsInitializeRegion(Command command)
        {
            return IsInitializeRegion(command.Cube.Max) && IsInitializeRegion(command.Cube.Min);
        }

        private static bool IsInitializeRegion(Vector vec)
        {
            return vec.X >= -50 && vec.X <= 50 && vec.Y >= -50 && vec.Y <= 50 && vec.Z >= -50 && vec.Z <= 50;
        }

        private static long CubeCount(Command command)
        {
            Vector min = command.Cube.Min, max = command.Cube.Max;
            return (command.On ? +1 : -1) * (1 + max.X - min.X) * (1 + max.Y - min.Y) * (1 + max.Z - min.Z);
        }
    }
}
