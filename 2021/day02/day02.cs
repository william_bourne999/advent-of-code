﻿namespace AdventOfCode2021
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day02
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day02/input").Select(ParseInstruction).ToList();

            // Part One - Follow the instructions and return the product of horizonal and vertical distances:
            int horizontal = 0, vertical  = 0;
            foreach (var (dir, val) in input)
            {
                switch (dir)
                {
                    case Dir.Down:    vertical   += val; break;
                    case Dir.Up:      vertical   -= val; break;
                    case Dir.Forward: horizontal += val; break;
                }
            }
            Console.WriteLine("Part One = {0}", horizontal * vertical);

            // Part Two - Use aim to follow instructions instead and return the product of the distances:
            int aim = horizontal = vertical = 0;
            foreach (var (dir, val) in input)
            {
                switch (dir)
                {
                    case Dir.Down:    aim        += val; break;
                    case Dir.Up:      aim        -= val; break;
                    case Dir.Forward: horizontal += val; vertical += aim * val; break;
                }
            }
            Console.WriteLine("Part Two = {0}", horizontal * vertical);
        }

        private enum Dir { Down, Up, Forward }
        private static (Dir, int) ParseInstruction(string str)
        {
            var idx = str.IndexOf(' ');
            var dir = str.Remove(idx);
            var val = str.Substring(idx + 1);
            return (Enum.Parse<Dir>(dir, true), int.Parse(val));
        }
    }
}
