﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day09
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day09/input");

            // Part One - Find all lowest points and sum their risk values:
            var partOne = 0;
            var maxY = input.Length - 1;
            var maxX = input[0].Length - 1;
            var basins = new List<(int X, int Y)>();
            for (var x = 0; x <= maxX; ++x)
            {
                for (var y = 0; y <= maxY; ++y)
                {
                    var val = input[y][x];
                    if (x > 0    && val >= input[y][x - 1]) continue;
                    if (x < maxX && val >= input[y][x + 1]) continue;
                    if (y > 0    && val >= input[y - 1][x]) continue;
                    if (y < maxY && val >= input[y + 1][x]) continue;
                    partOne += val + 1 - '0';
                    basins.Add((x, y));
                }
            }
            Console.WriteLine("Part One = {0}", partOne);

            // Part Two - Find the three largest basins and multiply their sizes:
            var sizes = basins.Select(basin => Search(input, basin.X, basin.Y, maxX, maxY)).ToList();
            var partTwo = sizes.OrderByDescending(x => x).Take(3).Aggregate((a, x) => a * x);
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static int Search(string[] input, int startX, int startY, int maxX, int maxY)
        {
            var initial = input[startY][startX];
            var visited = new HashSet<(int, int)> { (startX, startY) };
            Search(input, startX, startY, maxX, maxY, true,  initial, visited);
            Search(input, startX, startY, maxX, maxY, false, initial, visited);
            return visited.Count;
        }

        private static void Search(string[] input, int startX, int startY, int maxX, int maxY, bool inX, char prev, HashSet<(int, int)> visited)
        {
            Search(input, startX, startY, maxX, maxY, inX, -1, prev, visited);
            Search(input, startX, startY, maxX, maxY, inX, +1, prev, visited);
        }

        private static void Search(string[] input, int startX, int startY, int maxX, int maxY, bool inX, int dir, char prev, HashSet<(int, int)> visited)
        {
            var max = inX ? maxX : maxY;
            for (int c = (inX ? startX : startY) + dir; c >= 0 && c <= max; c += dir)
            {
                var x = inX ? c : startX;
                var y = inX ? startY : c;
                var val = input[y][x];
                if (val < prev || val == '9' || !visited.Add((x, y))) break;
                Search(input, x, y, maxX, maxY, !inX, prev = val, visited);
            }
        }
    }
}
