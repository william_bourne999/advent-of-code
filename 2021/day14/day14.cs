﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day14
    {
        public static void Main()
        {
            var input        = File.ReadAllLines("2021/day14/input");
            var template     = input[0];
            var instructions = input.Skip(2).ToDictionary(x => x.Remove(2), x => x[6]);
            var polymer      = new Dictionary<string, long>();
            for (int n = 1; n < template.Length; ++n) AddOrIncrement(polymer, string.Concat(template[n - 1], template[n]), 1);

            // Part One - Process the instructions ten times and return the score:
            for (int n = 0; n < 10; ++n) Process(ref polymer, instructions);
            Console.WriteLine("Part One = {0}", GetScore(polymer, template));

            // Part Two - Process the instructions thrity more times and return the score:
            for (int n = 0; n < 30; ++n) Process(ref polymer, instructions);
            Console.WriteLine("Part Two = {0}", GetScore(polymer, template));
        }

        private static void Process(ref Dictionary<string, long> polymer, Dictionary<string, char> instructions)
        {
            var updated = new Dictionary<string, long>();
            foreach (var (pair, count) in polymer)
            {
                var value = instructions[pair];
                var left  = string.Concat(pair[0], value);
                var right = string.Concat(value, pair[1]);
                AddOrIncrement(updated, left,  count);
                AddOrIncrement(updated, right, count);
            }
            polymer = updated;
        }

        private static long GetScore(Dictionary<string, long> polymer, string template)
        {
            var characters = new Dictionary<char, long> { { template.Last(), 1 } };
            foreach (var (pair, count) in polymer) AddOrIncrement(characters, pair[0], count);
            var counts = characters.Select(x => x.Value).OrderBy(x => x).ToList();
            return counts.Last() - counts.First();
        }

        private static void AddOrIncrement<T>(Dictionary<T, long> polymer, T key, long count)
        {
            if (polymer.ContainsKey(key)) polymer[key] += count;
            else polymer.Add(key, count);
        }
    }
}
