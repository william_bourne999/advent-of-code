﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    public static class Day16
    {
        private enum TypeID
        {
            Sum     = 0,
            Product = 1,
            Minimum = 2,
            Maximum = 3,
            Literal = 4,
            Greater = 5,
            Lesser  = 6,
            EqualTo = 7,
        }

        public static void Main()
        {
            var input  = File.ReadAllLines("2021/day16/input")[0];
            var binary = input.SelectMany(HexToBinary).ToArray();

            var index = 0;
            var versions = new List<long>();
            var literals = new List<long>();
            ParsePacket(binary, ref index, versions, literals);

            // Part One - Find the sum of all the version numbers:
            Console.WriteLine("Part One = {0}", versions.Sum());

            // Part Two - Find the result of the packets:
            Console.WriteLine("Part Two = {0}", literals.Sum());
        }

        private static string HexToBinary(char c)
        {
            return Convert.ToString(int.Parse(c.ToString(), NumberStyles.HexNumber), 2).PadLeft(4, '0');
        }

        private static void ParsePacket(char[] binary, ref int index, List<long> versions, List<long> literals)
        {
            // Parse the first 3 bits as the packet version number:
            versions.Add(Read3BitEncoded(binary, ref index));

            // The next 3 bits contain the packet type id:
            var typeID = (TypeID)Read3BitEncoded(binary, ref index);
            if (typeID == TypeID.Literal)
            {
                // Return the 5 bit encoded literal value:
                literals.Add(Read5BitEncoded(binary, ref index));
            }
            else
            {
                // Otherwise it is an operator packet:
                var subliterals = new List<long>();
                if (ReadN(binary, ref index, 1) == 1)
                {
                    // If the indicator bit is 1, the next 11 bits represent the number of sub-packets:
                    var subpackets = ReadN(binary, ref index, 11);
                    for (var n = 0; n < subpackets; ++n)
                    {
                        ParsePacket(binary, ref index, versions, subliterals);
                    }
                }
                else
                {
                    // The next 15 bits represent the length of the sub-packets:
                    var count = ReadN(binary, ref index, 15);
                    for (var final = index + count; index < final;)
                    {
                        ParsePacket(binary, ref index, versions, subliterals);
                    }
                }

                // Compute the result of the sub-packets:
                literals.Add(CalculatePacket(typeID, subliterals));
            }
        }

        private static long CalculatePacket(TypeID typeID, List<long> literals)
        {
            return typeID switch
            {
                TypeID.Sum     => literals.Sum(),
                TypeID.Product => literals.Aggregate((a, b) => a * b),
                TypeID.Minimum => literals.Min(),
                TypeID.Maximum => literals.Max(),
                TypeID.Greater => literals[0] >  literals[1] ? 1 : 0,
                TypeID.Lesser  => literals[0] <  literals[1] ? 1 : 0,
                TypeID.EqualTo => literals[0] == literals[1] ? 1 : 0,
                _ => 0,
            };
        }

        private static long Read3BitEncoded(char[] binary, ref int index)
        {
            return ReadN(binary, ref index, 3);
        }

        private static long Read5BitEncoded(char[] binary, ref int index)
        {
            long result = 0, value = 0x10;
            while ((value & 0x10) != 0)
            {
                value = ReadN(binary, ref index, 5);
                result = (result << 4) | (value & 0xF);
            }
            return result;
        }

        private static long ReadN(char[] binary, ref int index, int count)
        {
            int n = index;
            index += count;
            return binary.Skip(n).Take(count).Aggregate(0, (a, b) => (a << 1) | (b - '0'));
        }
    }
}
