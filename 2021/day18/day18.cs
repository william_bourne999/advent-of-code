﻿namespace AdventOfCode2021
{
    using System;
    using System.Collections.Immutable;
    using System.IO;
    using System.Linq;

    public static class Day18
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2021/day18/input").Select(SnailfishPair.Parse).ToArray();

            // Part One - Find the magnitude of the final sum of the input:
            var partOne = input.Aggregate((a, b) => a + b);
            Console.WriteLine("Part One = {0}", partOne.Magnitude);

            // Part Two - What is the largest magnitude you can get from any two additions:
            var range = Enumerable.Range(0, input.Length);
            var pairs = range.SelectMany(i => range.Where(j => i != j).Select(j => (i, j)));
            var partTwo = pairs.Max(x => (input[x.i]+input[x.j]).Magnitude);
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private class SnailfishPair : SnailfishComponent
        {
            public SnailfishComponent LHS { get; set; }
            public SnailfishComponent RHS { get; set; }
            public override long Magnitude => (3 * LHS.Magnitude) + (2 * RHS.Magnitude);
            public override string ToString() => $"[{LHS},{RHS}]";
            public override SnailfishPair Copy() => new() { LHS = LHS.Copy(), RHS = RHS.Copy() };

            public static SnailfishPair operator +(SnailfishPair left, SnailfishPair right)
            {
                return Reduce(new SnailfishPair { LHS = left.Copy(), RHS = right.Copy() });
            }

            public static SnailfishPair Parse(string str)
            {
                int n = 0;
                return new SnailfishPair().Parse(str, ref n);
            }

            private SnailfishPair Parse(string str, ref int n)
            {
                if (str[n] != '[') throw new InvalidOperationException($"Must be opening bracket N={n}");

                if (char.IsNumber(str[++n])) LHS = new SnailfishValue { Value = str[n] - '0' };
                else if (str[n] == '[')      LHS = new SnailfishPair().Parse(str, ref n);
                else throw new InvalidOperationException($"Expected number or opening bracket N={n}");

                if (str[++n] != ',') throw new InvalidOperationException($"Expected comma N={n}");

                if (char.IsNumber(str[++n])) RHS = new SnailfishValue { Value = str[n] - '0' };
                else if (str[n] == '[')      RHS = new SnailfishPair().Parse(str, ref n);
                else throw new InvalidOperationException($"Expected number or opening bracket N={n}");

                if (str[++n] != ']') throw new InvalidOperationException($"Expected closing bracket N={n}");
                return this;
            }

            private static SnailfishPair Reduce(SnailfishPair result)
            {
                var empty = ImmutableList<SnailfishPair>.Empty;
                while (ExplodePairs(result, empty) || SplitValues(result));
                return result;
            }

            private static bool ExplodePairs(SnailfishPair pair, ImmutableList<SnailfishPair> parents)
            {
                // Check if this is nested past the max depth and both parts are values:
                if (parents.Count > 3 && pair.LHS is SnailfishValue left && pair.RHS is SnailfishValue right)
                {
                    // Add the left and right values to the values either side:
                    var parent = parents[parents.Count - 1];
                    var isleft = ReferenceEquals(parent.LHS, pair);
                    var rvalue = isleft ? right.Value : left .Value;
                    var lvalue = isleft ? left .Value : right.Value;
                    AddValue(isleft ? parent.RHS : parent.LHS, rvalue, isleft);
                    AddValue(parents, lvalue, isleft);

                    // Replace the pair with a zero value:
                    if (isleft) parent.LHS = new SnailfishValue();
                    else        parent.RHS = new SnailfishValue();
                    return true;
                }

                // Check if either side is a pair that can be exploded its components:
                var subparents = parents.Add(pair);
                if (pair.LHS is SnailfishPair lhs && ExplodePairs(lhs, subparents)) return true;
                if (pair.RHS is SnailfishPair rhs && ExplodePairs(rhs, subparents)) return true;
                return false;
            }

            private static void AddValue(SnailfishComponent component, long value, bool left)
            {
                if (component is SnailfishValue leaf) leaf.Value += value;
                if (component is SnailfishPair  pair) AddValue(left ? pair.LHS : pair.RHS, value, left);
            }

            private static void AddValue(ImmutableList<SnailfishPair> parents, long value, bool left)
            {
                for (int n = parents.Count - 2; n >= 0; --n)
                {
                    var parent = parents[n];
                    var pair = parents[n + 1];
                    if (ReferenceEquals(left ? parent.RHS : parent.LHS, pair))
                    {
                        AddValue(left ? parent.LHS : parent.RHS, value, !left);
                        return;
                    }
                }
            }

            private static bool SplitValues(SnailfishPair pair)
            {
                return SplitValues(pair, true) || SplitValues(pair, false);
            }

            private static bool SplitValues(SnailfishPair pair, bool left)
            {
                var value = (left ? pair.LHS : pair.RHS) as SnailfishValue;
                if (value != null && value.Value > 9)
                {
                    var split = new SnailfishPair
                    {
                        LHS = new SnailfishValue { Value = (value.Value + 0) / 2 },
                        RHS = new SnailfishValue { Value = (value.Value + 1) / 2 },
                    };

                    if (left) pair.LHS = split;
                    else      pair.RHS = split;
                    return true;
                }

                return (left ? pair.LHS : pair.RHS) is SnailfishPair subpair && SplitValues(subpair);
            }
        }

        private class SnailfishValue : SnailfishComponent
        {
            public long Value { get; set; }
            public override long Magnitude => Value;
            public override string ToString() => $"{Value}";
            public override SnailfishValue Copy() => new() { Value = Value };
        }

        private abstract class SnailfishComponent
        {
            public abstract long Magnitude { get; }
            public abstract SnailfishComponent Copy();
        }
    }
}
