﻿namespace AdventOfCode.Utils
{
    using System.Numerics;

    public static class MathEx
    {
        public static T GreatestCommonDivisor<T>(T first, T second) where T : INumber<T>
        {
            while (second != T.Zero)
            {
                var temp = second;
                second = first % second;
                first = temp;
            }
            return first;
        }

        public static T LeastCommonMultiple<T>(T first, T second) where T : INumber<T>
        {
            return first / GreatestCommonDivisor(first, second) * second;
        }
    }
}
