﻿namespace AdventOfCode.Utils
{
    using System.Collections.Generic;

    public static class Extensions
    {
        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key) where TValue : new()
        {
            if (!dictionary.ContainsKey(key)) dictionary.Add(key, new());
            return dictionary[key];
        }

        public static T First<T>(this IReadOnlyList<T> list) => list[0];
        public static T Last <T>(this IReadOnlyList<T> list) => list[list.Count - 1];
    }
}
