﻿namespace AdventOfCode
{
    using System;

    public static class Startup
    {
        public static void Main(string[] args)
        {
            // Parse day and year from command line arguments:
            if (args == null || args.Length < 2 || !int.TryParse(args[0], out var year) || !int.TryParse(args[1], out var day))
            {
                year = DateTime.Today.Year;
                day  = DateTime.Today.Day;
            }

            // Call the main method for the given day and year:
            Console.WriteLine("Advent of Code {0} - Day {1}", year, day);
            typeof(Startup).Assembly.GetType($"AdventOfCode{year}.Day{day:D2}").GetMethod("Main").Invoke(null, null);
        }
    }
}
