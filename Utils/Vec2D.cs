﻿namespace AdventOfCode.Utils
{
    using System;

    public sealed record Vec2D(int X, int Y) : IComparable<Vec2D>
    {
        public static readonly Vec2D Up    = new(0, -1);
        public static readonly Vec2D Down  = new(0, +1);
        public static readonly Vec2D Left  = new(-1, 0);
        public static readonly Vec2D Right = new(+1, 0);

        public static Vec2D operator +(Vec2D a) => new(a.X, a.Y);
        public static Vec2D operator -(Vec2D a) => new(-a.X, -a.Y);
        public static Vec2D operator +(Vec2D a, Vec2D b) => new(a.X + b.X, a.Y + b.Y);
        public static Vec2D operator -(Vec2D a, Vec2D b) => new(a.X - b.X, a.Y - b.Y);
        public static Vec2D operator *(Vec2D a, int b) => new(a.X * b, a.Y * b);
        public static Vec2D operator *(int a, Vec2D b) => new(a * b.X, a * b.Y);

        public int CompareTo(Vec2D other)
        {
            var x = X.CompareTo(other.X);
            var y = Y.CompareTo(other.Y);
            return x != 0 ? x : y;
        }
    }
}
