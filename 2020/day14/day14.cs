﻿namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day14
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2020/day14/input");
            var memory = new Dictionary<long, long>();
            var bitmask = string.Empty;
            long SetBit(long val, int n, bool set) { return set ? val | (1L << (35 - n)) : val & ~(1L << (35 - n)); };

            // Part One: Bitmask sets the bits in the values, then sum all values:
            foreach (var line in input)
            {
                if (line.StartsWith("mask")) { bitmask = line.Substring(7); continue; }

                var address = long.Parse(line.Substring(4, line.IndexOf(']') - 4));
                var value   = long.Parse(line.Substring(line.LastIndexOf(' ')));
                for (int n = 0; n < bitmask.Length; ++n)
                {
                    if (bitmask[n] != 'X') value = SetBit(value, n, bitmask[n] == '1');
                }

                memory[address] = value;
            }

            Console.WriteLine("Part One: Sum of memory values {0}", memory.Values.Sum());

            // Part Two: Bitmask sets the 1 bits in the address, X is floating, then sum all values:
            memory.Clear();
            foreach (var line in input)
            {
                if (line.StartsWith("mask")) { bitmask = line.Substring(7); continue; }

                var address = long.Parse(line.Substring(4, line.IndexOf(']') - 4));
                var value   = long.Parse(line.Substring(line.LastIndexOf(' ')));
                for (int n = 0; n < bitmask.Length; ++n)
                {
                    if (bitmask[n] == '1') address = SetBit(address, n, true);
                }

                var addresses = new SortedSet<long> { address };
                for (int n = 0; n < bitmask.Length; ++n)
                {
                    if (bitmask[n] == 'X') addresses.UnionWith(addresses.SelectMany(x => new[] { SetBit(x, n, true), SetBit(x, n, false) }).ToList());
                }
                foreach (var addr in addresses) memory[addr] = value;
            }

            Console.WriteLine("Part One: Sum of memory values {0}", memory.Values.Sum());
        }
    }
}
