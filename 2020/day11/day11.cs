namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day11
    {
        public static void Main()
        {
            // Part One: Seats become empty with more than 3 neighbours, max move of 1:
            var input = File.ReadAllLines("2020/day11/input");
            Console.WriteLine("Part One: {0} occupied seats", ProcessSeats(input, 3, 1));

            // Part Two: Seats become empty with more than 4 neighbours, no max moves:
            Console.WriteLine("Part Two: {0} occupied seats", ProcessSeats(input, 4, -1));
        }

        public static int ProcessSeats(string[] input, int max, int moves)
        {
            var seats   = input.Select(s => s.ToCharArray()).ToArray();
            int rows    = seats.Length, cols = seats[0].Length;
            var changes = new List<(int, int, char)> { (0, 0, '.') };
            var directions = new[] { (-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1) };
            int GetCount(int row, int col, int drow, int dcol, int m)
            {
                for (row += drow, col += dcol; row > -1 && row < rows && col > -1 && col < cols && m-- != 0; row += drow, col += dcol)
                    switch (seats[row][col]) { case '#': return 1; case 'L': return 0; }
                return 0;
            };
            while (changes.Any())
            {
                changes.Clear();
                for (int row = 0; row < rows; ++row)
                {
                    for (int col = 0; col < cols; ++col)
                    {
                        char pos = seats[row][col];
                        if (pos == '.') continue;
                        int neighbours = directions.Sum(x => GetCount(row, col, x.Item1, x.Item2, moves));
                        if      (pos == 'L' && neighbours == 0 ) changes.Add((row, col, '#'));
                        else if (pos == '#' && neighbours > max) changes.Add((row, col, 'L'));
                    }
                }
                foreach (var (row, col, state) in changes) seats[row][col] = state;
            }

            // Return the number of occupied seats after the seats stabilize:
            return seats.Sum(x => x.Sum(c => c == '#' ? 1 : 0));
        }
    }
}
