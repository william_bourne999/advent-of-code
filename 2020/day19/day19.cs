﻿namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    public static class Day19
    {
        private static string[] _input;
        private static Dictionary<string, string> _data;

        public static void Main()
        {
            _input = File.ReadAllText("2020/day19/input").Split("\n\n");
            _data = new Dictionary<string, string>();
            _input[0].Split("\n").ToList().ForEach(x => Parse(x));

            Process();
            Console.ReadKey();
        }

        public static void Process()
        {
            Console.WriteLine($"Part 1 : {CountMatches()}");

            _data["8"] = "( 42 | 42 8 )";
            _data["11"] = "( 42 31 | 42 11 31 )";

            Console.WriteLine($"Part 2 : {CountMatches()}");
        }

        private static int CountMatches()
        {
            var regex = $"^{GenerateRegex()}$";
            return _input[1].Split("\n").Where(x => Regex.IsMatch(x, regex)).Count();
        }

        private static string GenerateRegex()
        {
            var current = _data["0"].Split(" ").ToList();
            while (current.Any(x => x.Any(y => char.IsDigit(y))) && current.Count() < 100000)
            {
                current = current.Select(x => _data.ContainsKey(x) ? _data[x] : x).SelectMany(x => x.Split(" ")).ToList();
            }
            current.Remove("8");
            current.Remove("11");

            return string.Join("", current);
        }

        private static void Parse(string line)
        {
            var colonDelimited = line.Split(":");
            var key = colonDelimited[0];

            var value = colonDelimited[1].Replace("\"", "").Trim();
            if (value.Contains("|"))
            {
                var delimited = value.Split("|");
                value = $"( {delimited[0]} | {delimited[1]} )";
            }

            _data.Add(key, value);
        }

        /*
        // Part One: Parse the expressions in order and sum the answers:
        var input = File.ReadAllText("2020/day19/input").Split("\n\n");

            // Parse the rules into the keys and requirements:
            var rules = input[0].Split('\n').Select(x => x.Split(':')).ToDictionary(
                x => x[0].Trim(),
                x => x[1].Replace("\"", string.Empty).Split('|'));

            // Build the map of acceptable strings for each rule:
            var completed = new Dictionary<string, string[]>();
            while (rules.Any())
            {
                foreach (var (key, values) in rules)
                {
                    if (values.All(s => s.All(c => c == 'a' || c == 'b' || c == ' ')))
                        completed.Add(key, values.Select(s => s.Replace(" ", string.Empty)).ToArray());
                }
                foreach (var key in completed.Keys) rules.Remove(key);

                // Search for rules that reference completed rules:
                var temp = new Dictionary<string, string[]>();
                foreach (var (key, values) in rules)
                {
                    var newvals = new List<string>();
                    for (int n = 0; n < values.Length; ++n)
                    {
                        var newval = string.Concat(" ", values[n], " ");
                        var newtmp = new List<string>();
                        foreach (var (completedKey, completedValues) in completed)
                        {
                            if (newval.Contains(" " + completedKey + " "))
                            {
                                foreach (var completedValue in completedValues)
                                {
                                    newtmp.Add(newval.Replace(completedKey, completedValue));
                                }
                            }
                        }

                        if (newtmp.Any()) newvals.AddRange(newtmp);
                        else              newvals.Add(newval);
                    }

                    temp.Add(key, newvals.ToArray());
                }
                rules = temp;
            }

            var rulezero = completed["0"];
            var distinct = rulezero.Distinct().ToArray();
            var data     = input[1].Split('\n');
            int matches  = distinct.Sum(x => data.Count(d => d == x));

            Console.WriteLine("Part Two: {0} strings match rule zero", matches);
            Console.ReadKey();
        }
        */
    }
}
