namespace AdventOfCode2020
{
    using System;
    using System.IO;

    public static class Day12
    {
        public static void Main()
        {
            // Part One: Find Manhattan distance after performing the ships movements:
            var input = File.ReadAllLines("2020/day12/input");
            Console.WriteLine("Part One: {0} distance from starting position", GetFerryDist(input, false, new Vector { X = 1 }));

            // Part Two: Find Manhattan distance by moving the ship and waypoint:
            Console.WriteLine("Part Two: {0} distance from starting position", GetFerryDist(input, true, new Vector { X = 10, Y = 1 }));
        }

        public static int GetFerryDist(string[] input, bool move_wp, Vector wp)
        {
            var invert = "SWR";
            var pos = new Vector();
            foreach (var line in input)
            {
                int value = int.Parse(line.Substring(1)) * (invert.Contains(line[0]) ? -1 : +1);
                switch (line[0])
                {
                    case 'N': case 'S': (move_wp ? wp : pos).Y += value; break;
                    case 'E': case 'W': (move_wp ? wp : pos).X += value; break;
                    case 'L': case 'R': wp = wp.Rotate(value);           break;
                    case 'F':           pos += value * wp;               break;
                }
            }
            return Math.Abs(pos.X) + Math.Abs(pos.Y);
        }

        public class Vector
        {
            public int X;
            public int Y;
            public static Vector operator *(int a, Vector b) { return new Vector { X = a * b.X, Y = a * b.Y }; }
            public static Vector operator +(Vector a, Vector b) { return new Vector { X = a.X + b.X, Y = a.Y + b.Y }; }
            public Vector Rotate(int deg)
            {
                int sin = (int)Math.Sin(Math.PI * deg / 180), cos = (int)Math.Cos(Math.PI * deg / 180);
                return new Vector { X = cos * X - sin * Y, Y = sin * X + cos * Y };
            }
        }
    }
}
