namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day04
    {
        public static void Main()
        {
            var input = File.ReadAllText("2020/day04/input").Split(new[] { "\n\n" }, StringSplitOptions.RemoveEmptyEntries);
            var ValidEye = new HashSet<string> { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };
            bool ValidHex(char c) { return c >= '0' && c <= '9' || c >= 'a' && c <= 'f'; }
            bool ValidInt(string str, int start, int length, int min, int max)
            {
                return str.Length >= start + length && int.TryParse(str.Substring(start, length), out var n) && n >= min && n <= max;
            }

            int validPartOne = 0;
            int validPartTwo = 0;
            foreach (var entry in input)
            {
                var parts = entry.Split(new[] { "\n", " " }, StringSplitOptions.RemoveEmptyEntries);
                var byr = parts.FirstOrDefault(s => s.StartsWith("byr:")) ?? string.Empty;
                var iyr = parts.FirstOrDefault(s => s.StartsWith("iyr:")) ?? string.Empty;
                var eyr = parts.FirstOrDefault(s => s.StartsWith("eyr:")) ?? string.Empty;
                var hgt = parts.FirstOrDefault(s => s.StartsWith("hgt:")) ?? string.Empty;
                var hcl = parts.FirstOrDefault(s => s.StartsWith("hcl:")) ?? string.Empty;
                var ecl = parts.FirstOrDefault(s => s.StartsWith("ecl:")) ?? string.Empty;
                var pid = parts.FirstOrDefault(s => s.StartsWith("pid:")) ?? string.Empty;

                // Part One: Passport is valid if it contains byr, iyr, eyr, hgt, hcl, ecl and pid:
                if (byr.Any() && iyr.Any() && eyr.Any() && hgt.Any() && hcl.Any() && ecl.Any() && pid.Any())
                    validPartOne++;

                // Part Two: Passport is valid if the fields pass the validation checks:
                bool validbyr = ValidInt(byr, 4, 4, 1920, 2002);
                bool validiyr = ValidInt(iyr, 4, 4, 2010, 2020);
                bool valideyr = ValidInt(eyr, 4, 4, 2020, 2030);
                bool validhgt = hgt.EndsWith("cm") && ValidInt(hgt, 4, 3, 150, 193) || hgt.EndsWith("in") && ValidInt(hgt, 4, 2, 59, 76);
                bool validhcl = hcl.Length == 11 && hcl[4] == '#' && hcl.Substring(5).All(ValidHex);
                bool validecl = ecl.Length == 7 && ValidEye.Contains(ecl.Substring(4));
                bool validpid = pid.Length == 13 && pid.Substring(4).All(char.IsDigit);
                if (validbyr && validiyr && valideyr && validhgt && validhcl && validecl && validpid)
                    validPartTwo++;
            }

            Console.WriteLine("Part One: {0} valid passports", validPartOne);
            Console.WriteLine("Part Two: {0} valid passports", validPartTwo);
        }
    }
}
