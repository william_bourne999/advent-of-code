﻿namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day17
    {
        public class Cell3D
        {
            public int X;
            public int Y;
            public int Z;
            public bool Alive;
            public Chunk3D Chunk;

            public Cell3D(int x, int y, int z, Chunk3D chunk)
            {
                X = x;
                Y = y;
                Z = z;
                Alive = false;
                Chunk = chunk;
            }

            public int GetNeighbours()
            {
                int count = 0;
                for (int x = X - 1; x <= X + 1; ++x)
                    for (int y = Y - 1; y <= Y + 1; ++y)
                        for (int z = Z - 1; z <= Z + 1; ++z)
                        {
                            if (x == X && y == Y && z == Z) continue;
                            if (x < 0 || y < 0 || z < 0) continue;
                            if (x >= Chunk.Size || y >= Chunk.Size || z >= Chunk.Size) continue;
                            if (Chunk.Cells[x][y][z].Alive) ++count;
                        }

                return count;
            }
        }

        public class Chunk3D
        {
            public int X;
            public int Y;
            public int Z;
            public int Size;
            public Dimension3D Dimension;
            public readonly Cell3D[][][] Cells;

            public Chunk3D(int x, int y, int z, int size, Dimension3D dimension)
            {
                X = x;
                Y = y;
                Z = z;
                Size = size;
                Dimension = dimension;

                Cells = new Cell3D[size][][];
                for (int i = 0; i < size; ++i)
                {
                    Cells[i] = new Cell3D[size][];
                    for (int j = 0; j < size; ++j)
                    {
                        Cells[i][j] = new Cell3D[size];
                        for (int k = 0; k < size; ++k)
                        {
                            Cells[i][j][k] = new Cell3D(i, j, k, this);
                        }
                    }
                }
            }

            public int CountCells()
            {
                int count = 0;
                for (int x = 0; x < Size; ++x)
                    for (int y = 0; y < Size; ++y)
                        for (int z = 0; z < Size; ++z)
                            if (Cells[x][y][z].Alive) ++count;
                return count;
            }
        }

        public class Dimension3D
        {
            public readonly Dictionary<int, Chunk3D> Chunks = new Dictionary<int, Chunk3D>();

            public Chunk3D GetChunk(int x, int y, int z)
            {
                int key = (x, y, z).GetHashCode();
                if (Chunks.TryGetValue(key, out var chunk)) return chunk;
                chunk = new Chunk3D(x, y, z, 200, this);
                Chunks.Add(key, chunk);
                return chunk;
            }

            public int CountCells()
            {
                int count = 0;
                foreach (var chunk in Chunks.Values) count += chunk.CountCells();
                return count;
            }
        }

        public static void Main()
        {
            var input = File.ReadAllLines("2020/day17/input");
            var cubes = new Dimension3D();
            var chunk = cubes.GetChunk(0, 0, 0);
            int rows = input.Length;
            int cols = input[0].Length;
            int startX = (chunk.Size - cols) / 2;
            int startY = (chunk.Size - rows) / 2;
            int startZ = chunk.Size / 2;
            for (int x = 0; x < cols; ++x)
                for (int y = 0; y < rows; ++y)
                    chunk.Cells[x + startX][y + startY][startZ].Alive = (input[y][x] == '#');

            var changes = new List<(Cell3D, bool)>();
            for (int n = 0; n < 6; ++n)
            {
                changes.Clear();

                for (int x = 0; x < chunk.Size; ++x)
                    for (int y = 0; y < chunk.Size; ++y)
                        for (int z = 0; z < chunk.Size; ++z)
                        {
                            var cell = chunk.Cells[x][y][z];
                            int neighbours = cell.GetNeighbours();
                            if (cell.Alive && (neighbours < 2 || neighbours > 3)) changes.Add((cell, false));
                            else if (!cell.Alive && neighbours == 3) changes.Add((cell, true));
                        }

                foreach (var (cell, alive) in changes) cell.Alive = alive;
                if (!changes.Any()) break;
            }

            int size = 100;
            var cells = new bool[size][][][];
            for (int x = 0; x < size; ++x)
            {
                cells[x] = new bool[size][][];
                for (int y = 0; y < size; ++y)
                {
                    cells[x][y] = new bool[size][];
                    for (int z = 0; z < size; ++z)
                    {
                        cells[x][y][z] = new bool[size];
                    }
                }
            }

            startX = (size - cols) / 2;
            startY = (size - rows) / 2;
            int startW = startZ = size / 2;
            for (int x = 0; x < cols; ++x)
                for (int y = 0; y < rows; ++y)
                    cells[x + startX][y + startY][startZ][startW] = (input[y][x] == '#');

            var changes4D = new List<(int, int, int, int, bool)>();
            for (int n = 0; n < 6; ++n)
            {
                changes4D.Clear();

                for (int x = 0; x < size; ++x)
                    for (int y = 0; y < size; ++y)
                        for (int z = 0; z < size; ++z)
                            for (int w = 0; w < size; ++w)
                            {
                                var alive = cells[x][y][z][w];
                                int neighbours = 0;
                                for (int i = x - 1; i <= x + 1; ++i)
                                    for (int j = y - 1; j <= y + 1; ++j)
                                        for (int k = z - 1; k <= z + 1; ++k)
                                            for (int l = w - 1; l <= w + 1; ++l)
                                            {
                                                if (i == x && j == y && k == z && l == w) continue;
                                                if (i  < 0 || j  < 0 || k  < 0 || l  < 0) continue;
                                                if (i >= size || j >= size || k >= size || l >= size) continue;
                                                if (cells[i][j][k][l]) ++neighbours;
                                            }

                                if (alive && (neighbours < 2 || neighbours > 3)) changes4D.Add((x, y, z, w, false));
                                else if (!alive && neighbours == 3) changes4D.Add((x, y, z, w, true));
                            }

                foreach (var (x, y, z, w, alive) in changes4D) cells[x][y][z][w] = alive;
                if (!changes4D.Any()) break;
            }

            int alive4D = 0;
            for (int x = 0; x < size; ++x)
                for (int y = 0; y < size; ++y)
                    for (int z = 0; z < size; ++z)
                        for (int w = 0; w < size; ++w)
                            if (cells[x][y][z][w]) ++alive4D;

            Console.WriteLine("Part One: 3D active cubes after six cycles: {0} [375]", cubes.CountCells());
            Console.WriteLine("Part One: 4D active cubes after six cycles: {0} [2192]", alive4D);
            Console.ReadKey();
        }
    }
}
