namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day07
    {
        public static void Main()
        {
            (string, int) ParseContents(string s)
            {
                int i = s.IndexOf(' '), j = s.IndexOf(" bag");
                return int.TryParse(s.Substring(0, i), out int n) ? (s.Substring(i + 1, j - i - 1), n) : (null, 0);
            }

            var bagmap = File.ReadAllLines("2020/day07/input").ToDictionary(
                x => x.Substring(0, x.IndexOf("bags") - 1),
                x => x.Substring(x.IndexOf("contain") + 8).Split(", ").Select(ParseContents).Where(a => a.Item2 > 0).ToDictionary(a => a.Item1, a => a.Item2));

            // Part One: Count how many bags can contain shiny gold bags:
            var allColours = new HashSet<string>();
            var previousColours = new HashSet<string> { "shiny gold" };
            while (previousColours.Any())
            {
                var newColours = new HashSet<string>(bagmap.Where(x => previousColours.Any(x.Value.ContainsKey)).Select(x => x.Key));
                allColours.UnionWith(newColours);
                previousColours = newColours;
            } 

            // Part Two: Count number of bags required to fill a shiny gold bag:
            int CountBags(string colour) => bagmap[colour].Sum(x => x.Value * (1 + CountBags(x.Key)));
            int totalBags = CountBags("shiny gold");

            Console.WriteLine("Part One: {0} bags can contain shiny gold bags", allColours.Count);
            Console.WriteLine("Part Two: {0} bags required to fill a shiny gold bag", totalBags);
        }
    }
}
