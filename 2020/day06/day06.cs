namespace AdventOfCode2020
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day06
    {
        public static void Main()
        {
            int countPartOne = 0, countPartTwo = 0;
            var input = File.ReadAllText("2020/day06/input").Split(new[] { "\n\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var entry in input)
            {
                // Part One: Count answers that anyone said yes to:
                for (char c = 'a'; c <= 'z'; ++c) countPartOne += entry.Contains(c) ? 1 : 0;

                // Part Two: Count answers than everyone said yes to:
                var people = entry.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Length;
                for (char c = 'a'; c <= 'z'; ++c) countPartTwo += entry.Count(x => x == c) == people ? 1 : 0;
            }

            Console.WriteLine("Part One: {0}", countPartOne);
            Console.WriteLine("Part Two: {0}", countPartTwo);
        }
    }
}
