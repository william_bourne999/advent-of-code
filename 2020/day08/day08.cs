namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day08
    {
        public static void Main()
        {
            // Part One: Run loop until it starts to repeat:
            var instructions = File.ReadAllLines("2020/day08/input");
            int accumulator = RunBootCode(instructions, out bool finished);
            Console.WriteLine("Part One: {0} before first repeat", accumulator);

            // Part Two: Change one jmp to nop or nop to jump so that it completes:
            for (int linechange = 0; linechange < instructions.Length && !finished; ++linechange)
            {
                var newinstructions = instructions.ToArray();
                var line = newinstructions[linechange];
                     if (line.StartsWith("nop")) newinstructions[linechange] = line.Replace("nop", "jmp");
                else if (line.StartsWith("jmp")) newinstructions[linechange] = line.Replace("jmp", "nop");
                accumulator = RunBootCode(newinstructions, out finished);
            }

            Console.WriteLine("Part Two: {0} after completion", accumulator);
        }

        private static int RunBootCode(string[] instructions, out bool finished)
        {
            int accumulator = 0;
            int linenumber  = 0;
            var visitedLines = new HashSet<int>();
            while (visitedLines.Add(linenumber) && linenumber < instructions.Length)
            {
                var instruction = instructions[linenumber];
                var argument = int.Parse(instruction.Substring(4));
                if      (instruction.StartsWith("acc")) { accumulator += argument; linenumber++; }
                else if (instruction.StartsWith("jmp")) { linenumber  += argument; }
                else if (instruction.StartsWith("nop")) { linenumber++; }
            }

            finished = (linenumber == instructions.Length);
            return accumulator;
        }
    }
}
