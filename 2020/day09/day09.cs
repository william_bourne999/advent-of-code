namespace AdventOfCode2020
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day09
    {
        public static void Main()
        {
            // Part One: Find first number that is not the sum of two distinct values from the previous 25 values:
            long weaksum = 0;
            var input = File.ReadAllLines("2020/day09/input").Select(long.Parse).ToList();
            for (int i = 0; i < input.Count - 25; ++i)
            {
                var value = input[i + 25];
                var preamble = input.Skip(i).Take(25).Distinct().ToList();

                bool valid = false;
                for (int n = 0; n < preamble.Count; ++n)
                    for (int m = n + 1; m < preamble.Count; ++m)
                        if (preamble[n] + preamble[m] == value)
                            valid = true;

                if (!valid)
                {
                    weaksum = value;
                    Console.WriteLine("Part One: {0} is first invalid number {1}", value, value == 1504371145);
                    break;
                }
            }

            // Part Two: Find the min and max of a set of contiguious numbers that sum to the answer to part one:
            bool found = false;
            for (int i = 0; i < input.Count && !found; ++i)
            {
                long sum = 0;
                for (int j = 0; j < input.Count - i && sum < weaksum && !found; ++j)
                {
                    var range = input.Skip(i).Take(j);
                    sum = range.Sum();
                    if (sum == weaksum)
                    {
                        found = true;
                        Console.WriteLine("Part Two: {0} is sum of smallest and largest", range.Min() + range.Max());
                    }
                }
            }
        }
    }
}
