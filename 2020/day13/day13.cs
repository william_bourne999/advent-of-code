namespace AdventOfCode2020
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day13
    {
        public static void Main()
        {
            var input    = File.ReadAllLines("2020/day13/input");
            var depart   = int.Parse(input[0]);
            var services = input[1].Split(',').Select((x, offset) => long.TryParse(x, out var id) ? (id, offset) : (0, 0)).Where(x => x.id > 0).ToList();
            var ordered  = services.Select(x => (x.id, time: (1 + (depart / x.id)) * x.id)).OrderBy(x => x.time).ToList();

            // Part One: Find bus which leaves earliest after the miminum departure time, then multiply the bus ID by the number of minutes required to wait:
            var firstBus = ordered.First();
            Console.WriteLine("Part One: Bus ID {0}", firstBus.id * (firstBus.time - depart));

            // Part Two: Find earliest timestamp T such that subsequent buses leave the seaport at N minutes past T:
            var step      = services[0].id;
            var timestamp = services[0].id;
            foreach (var (id, offset) in services.Skip(1))
            {
                while ((timestamp + offset) % id != 0) timestamp += step;
                step *= id;
            }
            Console.WriteLine("Part Two: Timestamp {0}", timestamp);
        }
    }
}
