namespace AdventOfCode2020
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day02
    {
        public static void Main()
        {
            int validPartOne = 0;
            int validPartTwo = 0;
            foreach (var line in File.ReadAllLines("2020/day02/input"))
            {
                var parts    = line.Split(new[] { '-', ':', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                var min      = int.Parse(parts[0]);
                var max      = int.Parse(parts[1]);
                var token    = parts[2][0];
                var password = parts[3];

                // Part one - Passwords require between min and max occurrences of the character to be valid:
                var count = password.Count(x => x == token);
                if (count >= min && count <= max) validPartOne++;

                // Part two - Password require the character exclusively at min or max position:
                var validmin = password.Length >= min && password[min - 1] == token;
                var validmax = password.Length >= max && password[max - 1] == token;
                if (validmin ^ validmax) validPartTwo++;

            }
            Console.WriteLine("Part One: {0} valid passwords", validPartOne);
            Console.WriteLine("Part Two: {0} valid passwords", validPartTwo);
        }
    }
}
