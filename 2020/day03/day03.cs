namespace AdventOfCode2020
{
    using System;
    using System.IO;

    public static class Day03
    {
        public static void Main()
        {
            var input  = File.ReadAllLines("2020/day03/input");
            int width  = input[0].Length;
            int height = input.Length;
            bool IsTree(int x, int y) { return input[y][x % width] == '#'; }
            int GetTreeCount(int vx, int vy)
            {
                int count = 0;
                for (int x = 0, y = 0; y < height; x += vx, y += vy) if (IsTree(x, y)) count++;
                return count;
            }

            // Count the number of hit trees for the set of different gradients:
            var count11 = GetTreeCount(1, 1);
            var count31 = GetTreeCount(3, 1);
            var count51 = GetTreeCount(5, 1);
            var count71 = GetTreeCount(7, 1);
            var count12 = GetTreeCount(1, 2);
            var product = count11 * count31 * count51 * count71 * count12;

            Console.WriteLine("Part One: {0} trees hit", count31);
            Console.WriteLine("Part Two: {0} is the product", product);
        }
    }
}
