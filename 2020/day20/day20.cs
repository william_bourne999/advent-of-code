﻿namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day20
    {
        public struct Pos
        {
            public int X;
            public int Y;

            public Pos(int x, int y) { X = x; Y = y; }

            public override string ToString() => $"({X}, {Y})";

            public static Pos operator+(Pos a, Pos b) { return new Pos(a.X + b.X, a.Y + b.Y); }
            public static Pos operator-(Pos a, Pos b) { return new Pos(a.X - b.X, a.Y - b.Y); }
            public static Pos operator+(Pos a) { return new Pos(+a.X, +a.Y); }
            public static Pos operator-(Pos a) { return new Pos(-a.X, -a.Y); }
        }

        public class Image
        {
            public long Id;
            public char[][] Data;

            public string LeftEdge   => string.Concat(Data.Select(x => x.Last()));
            public string RightEdge  => string.Concat(Data.Select(x => x.First()));
            public string TopEdge    => string.Concat(Data.First());
            public string BottomEdge => string.Concat(Data.Last());

            public void VerticalFlip()  => Array.Reverse(Data);
            public void HorizonalFlip() => Array.ForEach(Data, Array.Reverse);
            public void Rotate()
            {
                var copy = Data.Select(x => x.ToList()).ToList();
                for (int i = 0; i < Data.Length; ++i)
                    for (int j = 0; j < Data.Length; ++j)
                        Data[i][j] = copy[j][Data.Length - i - 1];
            }
            public string GetEdge(Pos off)
            {
                return off.X > 0 ? RightEdge : off.X < 0 ? LeftEdge : off.Y > 0 ? TopEdge : BottomEdge;
            }
        }

        public static readonly List<Image> Images = new List<Image>();
        public static readonly Dictionary<Pos, Image> Locations = new Dictionary<Pos, Image>();
        public static readonly Pos[] Offsets = new[] { new Pos(-1, 0), new Pos(1, 0), new Pos(0, -1), new Pos(0, 1) };

        public static void Main()
        {
            var input = File.ReadAllText("2020/day20/input").Split("\n\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (var section in input)
            {
                var id = long.Parse(section.Substring(5, 4));
                var data = section.Split('\n').Skip(1).Select(x => x.ToCharArray()).ToArray();
                Images.Add(new Image { Id = id, Data = data });
            }

            // Assume first tile is at (0, 0) and is in the correct orientation:
            Locations.Add(new Pos(0, 0), Images[0]);
            Images.RemoveAt(0);
            while (Images.Any()) LocateAdjacentTile();

            var minX     = Locations.Keys.Min(x => x.X);
            var maxX     = Locations.Keys.Max(x => x.X);
            var minY     = Locations.Keys.Min(x => x.Y);
            var maxY     = Locations.Keys.Max(x => x.Y);
            var botLeft  = Locations[new Pos(minX, minY)];
            var botRight = Locations[new Pos(maxX, minY)];
            var topLeft  = Locations[new Pos(minX, maxY)];
            var topRight = Locations[new Pos(maxX, maxY)];

            var res = string.Empty;
            foreach (var (pos, image) in Locations)
            {
                
            }

            // Part One: Multiply the four corner image IDs:
            var answer = botLeft.Id * botRight.Id * topLeft.Id * topRight.Id;
            Console.WriteLine("Part One: Product of corner tile IDs is {0} [27803643063307]", answer);

            // Part Two: Combine the images into a full image and search for sea monsters:
            var imagesize = topLeft.Data.Length - 2;
            var size = (1 + maxY - minY) * imagesize;
            var map = new char[size][];
            for (int n = 0; n < map.Length; ++n) map[n] = new char[size];
            foreach (var (pos, image) in Locations)
            {
                int startX = (pos.X - minX) * imagesize;
                int startY = (pos.Y - minY) * imagesize;
                for (int x = 0; x < imagesize; ++x)
                    for (int y = 0; y < imagesize; ++y)
                        map[startX + x][startY + y] = image.Data[x + 1][y + 1];
            }

            foreach (var line in map) Console.WriteLine(line);

            var full = new Image { Data = map };
            FindMonsters(full);

            int roughness = full.Data.Sum(x => x.Count(c => c == '#'));
            Console.WriteLine("Part Two: Sea roughness is {0}", roughness);
            Console.ReadKey();
        }

        public static void LocateAdjacentTile()
        {
            // Find an open edge in the map and see if any image fits:
            foreach (var (location, image) in Locations)
            {
                foreach (var offset in Offsets)
                {
                    var pos = location + offset;
                    if (!Locations.ContainsKey(pos))
                    {
                        // Find image that matches the edge in this offset direction:
                        foreach (var test in Images)
                        {
                            // Must try vertical/horizonal flip, 90 degree rotations:
                            for (int flip = 0; flip < 3; ++flip)
                            {
                                switch (flip)
                                {
                                    case 0: break;
                                    case 1: test.HorizonalFlip(); break;
                                    case 2: test.VerticalFlip (); break;
                                }

                                for (int rot = 0; rot < 5; ++rot)
                                {
                                    if (rot > 0) test.Rotate();
                                    if (test.GetEdge(-offset) == image.GetEdge(offset))
                                    {
                                        Locations.Add(pos, test);
                                        Images.Remove(test);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static bool FindMonsters(Image full)
        {
            var monster = new[] { "                  # ", "#    ##    ##    ###", " #  #  #  #  #  #   " };
            int monX = monster[0].Length, monY = monster.Length;
            for (int flip = 0; flip < 3; ++flip)
            {
                switch (flip)
                {
                    case 0: break;
                    case 1: full.HorizonalFlip(); break;
                    case 2: full.VerticalFlip (); break;
                }

                for (int rot = 0; rot < 5; ++rot)
                {
                    if (rot > 0) full.Rotate();

                    bool foundMonster = false;
                    for (int x = 0; x < full.Data.Length - monX; ++x)
                    {
                        for (int y = 0; y < full.Data.Length - monY; ++y)
                        {
                            bool monsterHere = true;
                            for (int i = 0; i < monX && monsterHere; ++i)
                            {
                                for (int j = 0; j < monY && monsterHere; ++j)
                                {
                                    var monchar = monster[monY - j - 1][i];
                                    var mapchar = full.Data[x + i][y + j];
                                    if (monchar == '#' && mapchar != '#')
                                    {
                                        monsterHere = false;
                                        break;
                                    }
                                }
                            }

                            if (monsterHere)
                            {
                                foundMonster = true;
                                for (int i = 0; i < monX; ++i)
                                    for (int j = 0; j < monY; ++j)
                                        if (monster[monY - j - 1][i] == '#') full.Data[x + i][y + j] = 'O';
                            }
                        }
                    }

                    if (foundMonster) return true;
                }
            }

            return false;
        }
    }
}
