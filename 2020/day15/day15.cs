﻿namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Day15
    {
        public static void Main()
        {
            var numbers = new List<int> { 15, 12, 0, 14, 3, 1 };
            var indices = numbers.Select((x, n) => (x, n)).ToDictionary(x => x.x, x => x.n);
            while (numbers.Count < 30000000)
            {
                var number = numbers[numbers.Count - 1];
                var next = indices.TryGetValue(number, out int index) ? numbers.Count - index - 1 : 0;
                indices[number] = numbers.Count - 1;
                numbers.Add(next);
            }

            Console.WriteLine("Part One: 2020th     number is {0}", numbers[2020     - 1]);
            Console.WriteLine("Part Two: 30000000th number is {0}", numbers[30000000 - 1]);
            Console.ReadKey(); 
        }
    }
}
