namespace AdventOfCode2020
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day01
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2020/day01/input").Select(int.Parse).ToList();

            // Part one - Find two numbers that sum to 2020 and return the product:
            for (int i = 0; i < input.Count; ++i)
                for (int j = i + 1; j < input.Count; ++j)
                    if (input[i] + input[j] == 2020)
                        Console.WriteLine("{0} + {1} == 2020 | {0} * {1} = {2}", input[i], input[j], input[i] * input[j]);

            // Part two - Find three numbers that sum to 2020 and return the product:
            for (int i = 0; i < input.Count; ++i)
                for (int j = i + 1; j < input.Count; ++j)
                    for (int k = j + 1; k < input.Count; ++k)
                        if (input[i] + input[j] + input[k] == 2020)
                            Console.WriteLine("{0} + {1} + {2} == 2020 | {0} * {1} * {2} = {3}", input[i], input[j], input[k], input[i] * input[j] * input[k]);
        }
    }
}
