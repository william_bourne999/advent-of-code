﻿namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day16
    {
        public static void Main()
        {
            var input = File.ReadAllText("2020/day16/input");
            var sections = input.Split("\n\n");

            var rules = new Dictionary<string, List<(int min, int max)>>();
            foreach (var line in sections[0].Split('\n'))
            {
                var index = line.IndexOf(':');
                var name  = line.Remove(index);
                var range = line.Substring(index + 1).Split(new[] { " ", "or", "-" }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
                var ranges = new List<(int, int)>();
                for (int n = 1; n < range.Count(); n += 2) ranges.Add((range[n - 1], range[n]));
                rules.Add(name, ranges);
            }

            // Part One: Find and discard invalid tickets:
            int ticketErrorRate = 0;
            var otherTickets = sections[2].Split('\n', StringSplitOptions.RemoveEmptyEntries).Skip(1).Select(x => x.Split(',').Select(int.Parse).ToList()).ToList();
            foreach (var ticket in otherTickets.ToList())
            {
                foreach (var value in ticket)
                {
                    bool valid = false;
                    foreach (var rule in rules)
                    {
                        foreach (var range in rule.Value)
                        {
                            valid |= value >= range.Item1 && value <= range.Item2;
                            if (valid) break;
                        }
                        if (valid) break;
                    }
                    if (!valid)
                    {
                        ticketErrorRate += value;
                        otherTickets.Remove(ticket);
                    }
                }
            }

            // Part Two: Determine which fields are which:
            var fields = new Dictionary<string, int>();
            var fieldValues = Enumerable.Range(0, rules.Count).Select(n => otherTickets.Select(x => x[n]).ToArray()).ToArray();
            var possibleRules = new Dictionary<int, List<string>>();

            int fieldIndex = 0;
            foreach (var values in fieldValues)
            {
                foreach (var rule in rules)
                {
                    bool valid = values.All(value => rule.Value.Any(x => value >= x.min && value <= x.max));
                    if (valid)
                    {
                        if (!possibleRules.ContainsKey(fieldIndex)) possibleRules.Add(fieldIndex, new List<string>());
                        possibleRules[fieldIndex].Add(rule.Key);
                    }
                }
                ++fieldIndex;
            }

            while (true)
            {
                var uniques = possibleRules.Where(x => x.Value.Count == 1).ToList();
                if (uniques.Count == 0) break;

                foreach (var unique in uniques)
                {
                    int field = unique.Key;
                    var rule  = unique.Value.Single();
                    fields.Add(rule, field);

                    foreach (var possibleRule in possibleRules)
                        possibleRule.Value.RemoveAll(x => x == rule);
                }
            }

            long departureProduct = 1;
            var myTicket = sections[1].Split('\n').Skip(1).Select(x => x.Split(',').Select(int.Parse).ToList()).Single();
            foreach (var field in fields)
            {
                if (field.Key.StartsWith("departure"))
                {
                    departureProduct *= myTicket[field.Value];
                }
            }

            Console.WriteLine("Part One: Ticket scanning error rate: {0} [30869]", ticketErrorRate);
            Console.WriteLine("Part Two: Product of departure values: {0} [4381476149273]", departureProduct);
        }
    }
}
