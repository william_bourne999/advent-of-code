namespace AdventOfCode2020
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day10
    {
        public static void Main()
        {
            // Part One: Find the nubmer of one and three jolt differences and return their product:
            var jolts = File.ReadAllLines("2020/day10/input").Select(int.Parse).OrderBy(x => x).ToArray();
            var diffs = jolts.Select((x, i) => jolts[i] - (i > 0 ? jolts[i-1] : 0)).Concat(new[] { 3 }).ToArray();
            Console.WriteLine("Part One: {0}", diffs.Count(x => x == 1) * diffs.Count(x => x == 3));

            // Part Two: Count the number of unqiue arrangements of adapters:
            // Any jump of threes or single ones can only have one combination.
            // Consequtive sets of N ones can be combined in the sum of all combinations of (N-1) items.
            long combinations = 1, ones = 0;
            foreach (var diff in diffs)
            {
                if (diff == 1) ++ones;
                if (diff == 3) { combinations *= ones == 4 ? 7 : ones == 3 ? 4 : ones == 2 ? 2 : 1; ones = 0; }
            }
            Console.WriteLine("Part Two: {0}", combinations);
        }
    }
}
