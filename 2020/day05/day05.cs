namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Day05
    {
        public static void Main()
        {
            var list = new SortedSet<int>();
            var input = File.ReadAllLines("2020/day05/input");
            foreach (var entry in input)
            {
                int seatID = 0;
                for (int i = 0; i < 10; ++i) if ("BR".Contains(entry[i])) seatID |= 1 << (9 - i);
                list.Add(seatID);
            }

            Console.WriteLine("Part One: Highest seat ID is {0}", list.Max);
            Console.WriteLine("Part Two: Missing seat ID is {0}", list.Zip(list.Skip(1), (a, b) => (a, b)).First(x => x.b - x.a != 1).a + 1);
            Console.ReadKey();
        }
    }
}
