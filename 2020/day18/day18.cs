﻿namespace AdventOfCode2020
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    public static class Day18
    {
        public enum Operation { Add, Mult }

        public static void Main()
        {
            // Part One: Parse the expressions in order and sum the answers:
            var input = File.ReadAllLines("2020/day18/input");
            var result = input.Sum(Calculate);
            Console.WriteLine("Part One: Sum of answers is {0}", result);

            // Part Two: Addition has higher precendence now, so surround adds with parentheses:
            result = input.Select(AddAdditionParentheses).Sum(Calculate);
            Console.WriteLine("Part Two: Sum of answers is {0}", result);

            Console.ReadKey();
        }

        public static string AddAdditionParentheses(string input)
        {
            var e2 = input.Replace(" ", "").ToList();

            int index = e2.IndexOf('+');
            while (index > 0)
            {
                int rearIndex;
                int frontIndex;
                if (e2[index + 1] == '(')
                {
                    int i = 1;
                    int parenCount = 0;
                    while ((index + i) < e2.Count)
                    {
                        if (e2[index + i] == '(') parenCount++;
                        if (e2[index + i] == ')') parenCount--;
                        if (parenCount == 0) break;
                        i++;
                    }
                    rearIndex = index + i;

                }
                else rearIndex = index + 2;
                e2.Insert(rearIndex, ')');

                if (e2[index - 1] == ')')
                {
                    int i = 1;
                    int parenCount = 0;
                    while ((index + i) >= 0)
                    {
                        if (e2[index - i] == '(') parenCount++;
                        if (e2[index - i] == ')') parenCount--;
                        if (parenCount == 0) break;
                        i++;
                    }
                    frontIndex = index - i;
                }
                else frontIndex = index - 1;
                e2.Insert(frontIndex, '(');

                index = e2.IndexOf('+', index + 2);
            }

            return string.Concat(e2);
        }

        public static long Calculate(string input)
        {
            int n = 0;
            return Calculate(input, ref n);
        }

        public static long Calculate(string input, ref int n)
        {
            long result = 0;
            var operation = Operation.Add;
            for (; n < input.Length; ++n)
            {
                var token = input[n];
                if (char.IsWhiteSpace(token)) continue;
                else if (token == '+') operation = Operation.Add;
                else if (token == '*') operation = Operation.Mult;
                else if (token == ')') break;
                else if (token == '(')
                {
                    ++n;
                    var value = Calculate(input, ref n);
                    switch (operation)
                    {
                        case Operation.Add:  result += value; break;
                        case Operation.Mult: result *= value; break;
                    }
                }
                else if (char.IsDigit(token))
                {
                    var number = string.Concat(input.Skip(n).TakeWhile(char.IsDigit));
                    n += number.Length - 1;
                    if (long.TryParse(number, out var value))
                    {
                        switch (operation)
                        {
                            case Operation.Add:  result += value; break;
                            case Operation.Mult: result *= value; break;
                        }
                    }
                }
            }

            return result;
        }
    }
}
