namespace AdventOfCode2022
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Day01
    {
        public static void Main()
        {
            var input = File.ReadAllLines("2022/day01/input");

            // For Part One, just find the maximum calories held by any of the elves:
            var elves = input.Select(GetIndex).Where(x => x.Value.Length > 0);
            var groups = elves.Select(GetIndex).GroupBy(x => x.Index - x.Value.Index);
            var calories = groups.Select(x => x.Sum(x => int.Parse(x.Value.Value))).ToList();
            Console.WriteLine("Part One = {0}", calories.Max());

            // For Part Two, sum the top three elves calories:
            var partTwo = calories.OrderDescending().Take(3).Sum();
            Console.WriteLine("Part Two = {0}", partTwo);
        }

        private static (T Value, int Index) GetIndex<T>(T value, int index) => (value, index);
    }
}
